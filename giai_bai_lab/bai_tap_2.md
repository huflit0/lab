# Bài tập 1,2

## Client

### Bước 1: định nghĩa các biến

```agsl
private static InetAddress host;
private static final int PORT = 1234;
private static Socket link = null;
private static Scanner input;
private static PrintWriter output;
private static DataOutputStream dataOutputStream = null;
private static DataInputStream dataInputStream = null;
```

### Bước 2: tạo hàm main

* Để kiểm tra host có tồn tại hay không thông qua try/cache. Nếu không tồn tại thì quăng lỗi, còn tồn tại thì sẽ chạy
  hàm accessServer()
* Sau đó kết nối đến server thông qua hàm accessServer()

```agsl
public static void main(String[] args) throws Exception {
    try {
        host = InetAddress.getLocalHost();
    } catch (UnknownHostException unknownHostException) {
        System.out.println("Error caused" + unknownHostException.getMessage());
        System.exit(1);

    }
    accessServer();
}
```

### Bước 3: tạo hàm access server

```agsl
private static void accessServer()
```

### Bước 4: trong hàm accessServer, kết nối đến server thông qua lớp Socket

```agsl
link = new Socket(host, PORT);
```

### Bước 5: Khởi tạo các input, output trong hàm accessServer, mục đích nhận và truyền dữ liệu

```agsl
input = new Scanner(link.getInputStream());
output = new PrintWriter(link.getOutputStream(), true);
```

### Bước 6: Khởi tạo menu

* do/while để thực hiện tạo menu, dùng do while vì nó sẽ chạy ít nhất 1 lần. Nếu choice != 3 sẽ dừng lại.
* ```output.println(choice);``` để gửi số case thứ tự case để server thực hiện thao tác tương ứng.
* Ở case 1, sẽ thực hiện nhập số nguyên tố và gửi lên server, sau đó sẽ đợi response kết quả từ server.
* Ở case 2, sẽ thực hiện nhập đường dẫ lưu file, và gọi hàm sendFle() kèm theo đường dẫn và Socket
* Ở case 3, sẽ in ra chữ đóng kết nối.

```agsl
Scanner scanner = new Scanner(System.in);
int choice;
Scanner userEntry = new Scanner(System.in);
String message, response;
do {
        System.out.println("\nMenu:");
        System.out.println("1. Kiểm tra số nguyên tố");
        System.out.println("2. Gửi file");
        System.out.println("3. Exit");
        System.out.print("Enter your choice: ");
        choice = scanner.nextInt();
        output.println(choice);
        switch (choice) {
            case 1:
                System.out.print("Nhập số nguyên tố: ");
                message = userEntry.nextLine();
                output.println(message);
                response = input.nextLine();
                System.out.println("\nSERVER > " + response);
                break;
            case 2:
                Scanner scan = new Scanner(System.in);
                System.out.print("Nhập đường dẫn file: ");
                String filePath = scan.next();
                sendFile(filePath, link);
                break;
            case 3:
                message = "CLOSE";
                output.println(message);
                break;
            default:
                System.out.println("Invalid choice. Please try again.");
        }
} while (choice != 3);
```

### Bước 7: Hàm sendFile() ở case 2 bước 6.

* Hàm này bước đầu tiên sẽ kiểm tra file có tồn tại hay không.
* Sau đó gửi tên file vào server bằng lệnh out.println(file.getName()).
* Tiếp theo đọc từng dòng của file thông qua lớp Scanner và gửi vào server
* Sau khi gửi xong sẽ in ra thông báo gửi thành công.

```agsl
private static void sendFile(String filePath, Socket clientSocket) throws FileNotFoundException, IOException {
    File file = new File(filePath);
    if (!file.exists()) {
        System.out.println("Error: File not found: " + filePath);
        System.exit(1);
    }

    try (FileInputStream fis = new FileInputStream(file); Scanner reader = new Scanner(fis); PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true) // Autoflush) {
        // Send file name to server
        out.println(file.getName());

        while (reader.hasNextLine()) {
            String line = reader.nextLine();
            out.println(line); // Send each line of the file
        }

        System.out.println("File sent successfully!");
    }

    output.println("Done");
}
```

## Server

### Bước 1: Tạo các biến

```agsl
private static ServerSocket serverSocket;
private static final int PORT = 1234;
private static Socket link = null;
private static Scanner input;
private static PrintWriter output;
private static DataOutputStream dataOutputStream = null;
private static DataInputStream dataInputStream = null;
```

### Bước 2: Tạo hàm main

* Bước đầu sẽ mở port để client kết nối vào thông qua lớp ServerSocket.
* while(true) để thực hiện chấp nhận kết nối liên tục không gián đoạn.
* hàm handleClient() để handle việc client nhập case số mấy để thực hiện thao tác tương ứng.

```agsl
public static void main(String[] args) throws Exception {
    System.out.println("OPENING PORT : " + PORT + "\n");
    while (true) {
        try {
            // Bước 1: server mở port để lắng nghe
            serverSocket = new ServerSocket(PORT);
        } catch (IOException ioException) {
            System.out.println("UNABLE TO ATTACH TO PORT ! EXCEPTION : " + ioException.getMessage());
            System.exit(1);
        }
        do {
            // Các bước 2->5
            handleClient();
        } while (true);
    }
}
```

### Bước 3: tạo hàm handleClient()

* Đầu tiên sẽ chấp nhận kết nối thông qua hàm accept() của lớp ServerSocket.
* Sau đó khởi tạo input, output để nhận và gửi.
* ```String message = input.nextLine();``` dùng để đọc thông tin đầu tiên mà client gửi, ở đây sẽ là số thứ tự case.
* Ở client là do/while có điểm dừng, server thì sẽ chạy liên tục không dừng nên thực hiện while(true)

#### Case 1

* Ở case 1, tin nhắn nhận vào sẽ là số nguyên ```message = input.nextLine();```. Sau đó sẽ thực hiện chạy hàm kiểm tra
  số đó có phải số nguyên tố hay không, phải chuyển về số nguyên thông qua ```Integer.parseInt(message)``` vì lúc nhận
  là string. Sau đó sẽ gửi kết quả trở về client bằng ```output.println()```

#### Case 2

* Ở case 2, nhận thông tin tên file thông qua lệnh ```String fileName = input.nextLine();```
* Sau đó chạy hàm lưu file ```receiveFile(fileName);```

```agsl
private static void handleClient() throws Exception {
    try {
        // Bước 2: chấp nhận kết nối từ client
        link = serverSocket.accept();

        // bước 3: lấy input từ client và ghi ra ngoài
        input = new Scanner(link.getInputStream());
        output = new PrintWriter(link.getOutputStream(), true);

        int numMessages = 0;
        // Bước 4: Lấy thông tin message từ client
        String message = input.nextLine();
        while (true) {
            System.out.println("MESSAGE RECEIVED.");
            numMessages++;
            switch (message) {
                case "1":
                    // Đoc message tiếp theo
                    message = input.nextLine();
                    // Kiểm tra số nguyên tố và gửi message đến client
                    if (isPrime(Integer.parseInt(message))) {
                        // Gửi message đến client
                        output.println("MESSAGE NUM " + numMessages + " : " + message + " là số nguyên tố");
                    } else {
                        output.println("MESSAGE NUM " + numMessages + " : " + message + " không phải là số nguyên tố");
                    }
                    break;
                case "2":
                    String fileName = input.nextLine();
                    receiveFile(fileName);
                    break;
                case "CLOSE":
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
            if (input.hasNextLine()) {
                message = input.nextLine();
            } else {
                // Handle end-of-input here (e.g., break or prompt user)
                System.err.println("Reached end of input. Connection closed?");
                break;
            }
        }
        output.println(numMessages + " messages received");
    } catch (IOException ioException) {
        ioException.printStackTrace();
        System.out.println(ioException.getMessage());

    } finally {
        try {
            System.out.println("\n* CLOSING CONNECTION . . . *");
            // Bước 5: đóng kết nối
            link.close();
        } catch (IOException e) {
            System.out.println("UNABLE TO DISCONNECT ! : " + e.getMessage());
            System.exit(1);
        }
    }
}
```

### Bước 4: Tạo hàm rereceiveFile()

* Hàm này có chức năng đọc toàn bộ dữ liệu nhận được thông qua ```String line = input.nextLine();```
* Sau đó lưu lại những gì đọc được ```fos.write(line.getBytes());```
* ```while (input.hasNextLine())``` có chức năng kiểm tra còn dữ liệu được gửi từ client hay không. Nếu còn thì tiếp tục
  ghi, còn không còn thì in ra kết quả ```System.out.println("File received successfully!");```

```agsl
private static void receiveFileNew(String fileName) throws Exception {
    try {
        input = new Scanner(link.getInputStream());
        output = new PrintWriter(link.getOutputStream(), true); // Autoflush

        // Create a new file on the server side
        File file = new File(fileName);
        FileOutputStream fos = new FileOutputStream(file);

        System.out.println("Receiving file: " + fileName);

        while (input.hasNextLine()) {
            String line = input.nextLine();
            output.println("ACK"); // Send acknowledgment for each line (optional)
            fos.write(line.getBytes()); // Write line content to file
        }
        System.out.println("File received successfully!");

    } catch (IOException e) {
        System.err.println("Error receiving or saving file: " + e.getMessage());
        output.println("error"); // Send error message to client (optional)
    }
}
```

