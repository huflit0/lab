# Bài tập 4

## Tạo hàm main()

* Trong hàm main có một số chức năng sau: tải file, tải html, whois, và thoát
* Case 1 sẽ thực hiện tải file của hàm ```downloadFile``` trong ```HttpDownloader``` và truyền vào URL của file và thư
  mục lưu file ```HttpDownloader.downloadFile(fileURL, saveDir);```
* Case 2 sẽ thực hiện tải html bằng hàm ```getHtml()``` trong lớp ```HttpDownloader```
* Case 3 sẽ thực hiện kiểm tra domain thông qua hàm ```whoIs()``` trong lớp ```HttpDownloader```

```java
public class Main {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        int choice;
        do {
            System.out.println("1. Download file");
            System.out.println("2. Get Html");
            System.out.println("3. Whois");
            System.out.println("4. Exit");
            System.out.print("choose: ");
            choice = sc.nextInt();

            switch (choice) {
                case 1:
                    String fileURL = "https://www.simplilearn.com/ice9/free_resources_article_thumb/what_is_image_Processing.jpg";
                    String saveDir = "C:\\Users\\phang\\Desktop\\huflit\\Huflit\\src\\main\\java\\Lab\\Buoi_4";

                    HttpDownloader.downloadFile(fileURL, saveDir);
                    System.out.println("Download completed");
                    break;
                case 2:
                    System.out.print("Enter Url");
                    String urlString = sc.next();
                    HttpDownloader.getHtml(urlString);
                    break;
                case 3:
                    System.out.print("Enter domain: ");
                    String domain = sc.next();
                    HttpDownloader.whoIs(domain);
                    break;
                default:
                    break;
            }
        } while (choice < 4);

        System.out.print("Done!");
    }
}
```

## Tạo lớp HttpDownloader

### Hàm downloadFile()

* Sử dụng lớp URL và HttpURLConnection để thực hiện tạo kết nối đến URL

```java
URL url = new URL(fileURL);
HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
httpConn.

setRequestMethod("GET");
httpConn.

connect();
```

* Kiểm tra xem URL có tồn tại hay không

```java
int responseCode = httpConn.getResponseCode();
if(responseCode !=HttpURLConnection.HTTP_OK){
        throw new

IOException("Error downloading file: "+responseCode);
}
```

* Thực hiện lấy tên file dựa trên header ```Content-Disposition```. Nếu header không có ```Content-Disposition``` thì
  file name dựa trên phần từ cuối cùng của URL (ví dụ: http://abc/test.jpg thì filename sẽ là test.jpg)

```java
String fileName = "";
String disposition = httpConn.getHeaderField("Content-Disposition");
String contentType = httpConn.getContent().toString();
int contentLength = httpConn.getContentLength();
long fileSize = httpConn.getContentLengthLong();

if(disposition !=null){
int index = disposition.indexOf("filename=");
    if(index >0){
fileName =disposition.

substring(index +10, disposition.length() -1);
        }
        }else{
fileName =fileURL.

substring(fileURL.lastIndexOf("/") +1,fileURL.

length());
        }
```

* Lưu file

```java
InputStream inputStream = httpConn.getInputStream();
String saveFilePath = saveDir + File.separator + fileName;
FileOutputStream outputStream = new FileOutputStream(saveFilePath);
int bytesRead;
long totalDownloaded = 0;

byte[] buffer = new byte[BUFFER_SIZE];
while((bytesRead =inputStream.

read(buffer))!=-1){
        outputStream.

write(buffer, 0,bytesRead);

// in trạng thái tải file bao nhiêu phần trăm
totalDownloaded +=bytesRead;
    if(fileSize >0){
int progress = (int) ((totalDownloaded * 100) / fileSize);
        System.out.

println("Downloaded "+progress +"%");
    }
            }
            outputStream.

close();
httpConn.

disconnect();
```

#### Tổng hợp code của hàm downloadFile()

```java
public static void downloadFile(String fileURL, String saveDir) throws IOException {
    URL url = new URL(fileURL);
    HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
    httpConn.setRequestMethod("GET");
    httpConn.connect();

    int responseCode = httpConn.getResponseCode();
    if (responseCode != HttpURLConnection.HTTP_OK) {
        throw new IOException("Error downloading file: " + responseCode);
    }

    String fileName = "";
    String disposition = httpConn.getHeaderField("Content-Disposition");
    String contentType = httpConn.getContent().toString();
    int contentLength = httpConn.getContentLength();
    long fileSize = httpConn.getContentLengthLong();

    if (disposition != null) {
        int index = disposition.indexOf("filename=");
        if (index > 0) {
            fileName = disposition.substring(index + 10, disposition.length() - 1);
        }
    } else {
        fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1, fileURL.length());
    }

    System.out.println("Content-Type: " + contentType);
    System.out.println("Content-Disposition: " + disposition);
    System.out.println("Content-Length: " + contentLength);

    System.out.println("fileName: " + fileName);


    // save file
    InputStream inputStream = httpConn.getInputStream();
    String saveFilePath = saveDir + File.separator + fileName;
    FileOutputStream outputStream = new FileOutputStream(saveFilePath);
    int bytesRead;
    long totalDownloaded = 0;

    byte[] buffer = new byte[BUFFER_SIZE];
    while ((bytesRead = inputStream.read(buffer)) != -1) {
        outputStream.write(buffer, 0, bytesRead);

        // Print download progress 
        totalDownloaded += bytesRead;
        if (fileSize > 0) {
            int progress = (int) ((totalDownloaded * 100) / fileSize);
            System.out.println("Downloaded " + progress + "%");
        }
    }
    outputStream.close();
    httpConn.disconnect();
}
```

## Hàm getHtml()

* Sử dụng lớp URL và URLConnection để thực hiện kết nối đến URL

```java
URL url = new URL(urlString);
URLConnection connection = url.openConnection();
```

* Tạo một stream

```java
BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
```

* Lưu toàn bộ nội dung HTML

```agsl
List<String> htmlContent = new ArrayList<String>();
String line;
while ((line = reader.readLine()) != null) {
    htmlContent.add(line);
}
reader.close();

System.out.println(String.join("\n", htmlContent));
```

#### Tổng hợp code hàm getHtml()

```java
public static void getHtml(String urlString) throws IOException {
    URL url = new URL(urlString);
    URLConnection connection = url.openConnection();

    // Open an input stream to read the HTML content
    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

    List<String> htmlContent = new ArrayList<String>();
    String line;
    while ((line = reader.readLine()) != null) {
        htmlContent.add(line);
    }
    reader.close();

    System.out.println(String.join("\n", htmlContent));
}
```

## Hàm whoIs()

* Kết nối Socket đến whois
  ```Socket socket = new Socket("whois.internic.com", port)```
* Tạo output để gửi đến server whois

```agsl 
OutputStreamWriter writer = new OutputStreamWriter(socket.getOutputStream());
writer.write("WHOIS "+domain +"\r\n");
writer.flush();
```

* Đọc toàn bộ dữ liệu nhận được từ server whois

```agsl
BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
String line;
while ((line = reader.readLine()) != null) {
    System.out.println(line);
}
```

#### Tổng hợp code hàm whoIs()

```java
public static void whoIs(String domain) throws IOException {
    // Default WHOIS server port
    int port = 43;

    // Connect to the WHOIS server
    try (Socket socket = new Socket("whois.internic.com", port)) {
        // Send the WHOIS query
        OutputStreamWriter writer = new OutputStreamWriter(socket.getOutputStream());
        writer.write("WHOIS " + domain + "\r\n");
        writer.flush();

        // Read the response from the WHOIS server
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
    }
}
```