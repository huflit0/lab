# Bài tập 1: Làm SHA256 và Base64

## Client

### Bước 1: nhập thông điệp muốn mã hoá

```java
System.out.print("Nhập thông điệp: ");

Scanner scan = new Scanner(System.in);
String message = scan.nextLine();
```

### Bước 2: Mã hóa thông điệp bằng SHA-256

* Lớp MessageDigest thường được dụng để sử dụng hash

```java
MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
byte[] digest = messageDigest.digest(message.getBytes());
```

### Bước 3: In ra chuỗi sau khi SHA-256

```
System.out.print("Chuỗi sau khi SHA-256: ");
StringBuilder sb = new StringBuilder();
for (byte b : digest) {
    sb.append(String.format("%02x", b));
}
System.out.println(sb.toString());
```

### Bước 4: Chuyển SHA-256 sang Base64

```java
String base64Encoded = Base64.getEncoder().encodeToString(digest);
```

### Bước 5: In ra chuỗi sau khi Base64

```java
System.out.println("Chuỗi sau khi Base64: "+base64Encoded);
```

### Bước 6: Kết nối đến server và gửi chuỗi Base64 đến server

```
Socket socket = new Socket("localhost", 5000);

PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
printWriter.println(base64Encoded);
printWriter.flush();
```

### Bước 7: Đóng kết nối

```java
socket.close();
```

### Code đầy đủ

```java
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.security.*;
import java.util.Base64;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        // Nhập thông điệp từ người dùng
        System.out.print("Nhập thông điệp: ");
        Scanner scan = new Scanner(System.in);
        String message = scan.nextLine();
        // Mã hóa thông điệp bằng SHA-256
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        byte[] digest = messageDigest.digest(message.getBytes());

        // In ra chuỗi sau khi SHA-256
        System.out.print("Chuỗi sau khi SHA-256: ");
        StringBuilder sb = new StringBuilder();
        for (byte b : digest) {
            sb.append(String.format("%02x", b));
        }
        System.out.println(sb.toString());

        // Chuyển đổi sang Base64
        String base64Encoded = Base64.getEncoder().encodeToString(digest);

        // In ra chuỗi sau khi Base64
        System.out.println("Chuỗi sau khi Base64: " + base64Encoded);

        // Kết nối đến server và gửi chuỗi BASE64
        Socket socket = new Socket("localhost", 5000);
        PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
        printWriter.println(base64Encoded);
        printWriter.flush();

        // Đóng kết nối
        socket.close();
    }
}
```

## Server

### Bước 1: Tạo ServerSocket để lắng nghe kết nối

```java
ServerSocket serverSocket = new ServerSocket(5000);
```

### Bước 2: Chấp nhận kết nối từ client

```java
Socket socket = serverSocket.accept();
```

### Bước 3: Nhận chuỗi Base64 từ client

```java
InputStream inputStream = socket.getInputStream();
BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
String messageFromClient = reader.readLine();
```

### Bước 4: Giải mã chuỗi BASE64 sang SHA-256

```java
byte[] decodedBytes = Base64.getDecoder().decode(messageFromClient);
```

### Bước 5: In ra SHA256

* ```%02x``` là in ra 2 ký tự, ```x``` ở đây là dạng hex.

```
StringBuilder sb = new StringBuilder();
for (byte b : decodedBytes) {
    sb.append(String.format("%02x", b));
}
System.out.println("Chuỗi SHA-256: " + sb.toString());
```

### Bước 6: In ra base64 từ client gửi lên

```java
System.out.println("Chuỗi BASE64: "+messageFromClient);
```

### Bước 7: Đóng kết nối

```java
socket.close();
```

### Code đầy đủ

```java
public class Server {
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        // Tạo ServerSocket để lắng nghe kết nối
        ServerSocket serverSocket = new ServerSocket(5000);

        while (true) {
            // Chấp nhận kết nối từ client
            Socket socket = serverSocket.accept();

            // Nhận chuỗi BASE64 từ client
            InputStream inputStream = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String messageFromClient = reader.readLine();

            // Giải mã chuỗi BASE64 sang SHA-256
            byte[] decodedBytes = Base64.getDecoder().decode(messageFromClient);

            // In ra SHA256
            StringBuilder sb = new StringBuilder();
            for (byte b : decodedBytes) {
                sb.append(String.format("%02x", b));
            }
            System.out.println("Chuỗi SHA-256: " + sb.toString());

            // In ra base64 từ client gửi lên
            System.out.println("Chuỗi BASE64: " + messageFromClient);

            // Đóng kết nối
            socket.close();
        }
    }
}
```

# Bài tập 2: Ký số

* Đầu tiên phải sinh ra cặp key pair ```https://cryptotools.net/rsagen```
* Sau đó chuyển đổi private-key sang pkcs8

## Client

### Bước 1: tạo hàm ký

* Client sẽ đọc private-key từ file ```String pemString = readFile(privateKeyFile);```
* ```pemString.replace()``` sẽ chuyển tất cả về 1 dòng và loại bỏ các header của pivate-key
* Private-key sẽ là 1 dòng ở dạng Base64, lúc này sẽ decode base64 của
  private-key ```byte[] decodedPrivateKey = Base64.getDecoder().decode(privateKeyPEM);```
* Sau đó sẽ thực hiện luồng ký dữ liệu.

```java
public static byte[] sign(String privateKeyFile, String message) throws Exception {
    //openssl genrsa -out private.pem 1024
    //openssl rsa -in private.pem -pubout -outform PEM -out public_key.pem
    //openssl pkcs8 -topk8 -inform PEM -in private.pem -out private_key.pem -nocrypt

    String pemString = readFile(privateKeyFile);

    // Remove header, footer, and newline characters

    String privateKeyPEM = pemString.replace(
                    "-----BEGIN PRIVATE KEY-----", ""
            )
            .replaceAll("\\n", "")
            .replace("-----END PRIVATE KEY-----", "");

    // Base64 decode the key data
    byte[] decodedPrivateKey = Base64.getDecoder().decode(privateKeyPEM);
    PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(decodedPrivateKey);
    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
    System.out.println(">>>>>>>>>" + privateKey);
    // Tạo đối tượng chữ ký
    Signature signature = Signature.getInstance("SHA256withRSA");

    // Khởi tạo chữ ký với private key
    signature.initSign(privateKey);

    // Cập nhật dữ liệu cần ký
    signature.update(message.getBytes());

    // Tạo chữ ký
    return signature.sign();
}
```

### Bước 2: gửi dữ liệu đã ký vào server

* Chạy hàm ký ở bước 1 ```byte[] signatureBytes = sign(privateKeyFile, message);```
* Sau đó gửi dữ liệu vào server ```printWriter.println(message);```

```java
public static void BT2() throws Exception {
    String message = "abc";
    String privateKeyFile = "/Users/apple/huflit/network programing/code/advanced-network-programming/src/main/java/Lab/Buoi_9/Bt2/private_key_pkcs8.pem";

    try {
        byte[] signatureBytes = sign(privateKeyFile, message);

        // Kết nối đến server và gửi chuỗi BASE64
        Socket socket = new Socket("localhost", 5001);
        PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
        //System.out.println(Arrays.toString(signatureBytes));
        printWriter.println(Base64.getEncoder().encodeToString(signatureBytes));
        //byte[] decoded = Base64.getDecoder().decode(Base64.getEncoder().encodeToString(signatureBytes));
        //System.out.println(Arrays.toString(decoded));
        printWriter.println(message);
        printWriter.flush();

        // Đóng kết nối
        socket.close();

    } catch (Exception e) {
        e.printStackTrace();
    }
}
```

### Code đầy đủ

```java
public class Client {
    // Có thể dùng lớp FileUtils hoặc tự định nghĩa hàm đọc file
    public static String readFile(String filePath) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (FileReader fr = new FileReader(filePath)) {
            int c;
            while ((c = fr.read()) != -1) {
                sb.append((char) c);
            }
        }
        return sb.toString();
    }

    public static byte[] sign(String privateKeyFile, String message) throws Exception {
        //openssl genrsa -out private.pem 1024
        //openssl rsa -in private.pem -pubout -outform PEM -out public_key.pem
        //openssl pkcs8 -topk8 -inform PEM -in private.pem -out private_key.pem -nocrypt
        // Read PEM file content as a string (replace with your actual reading method)
        //String pemString = readFile("/Users/apple/huflit/network programing/code/advanced-network-programming/src/main/java/Lab/Buoi_9/Bt2/private_key_pkcs8.pem");

        String pemString = readFile(privateKeyFile);

        // Remove header, footer, and newline characters

        String privateKeyPEM = pemString.replace(
                        "-----BEGIN PRIVATE KEY-----", ""
                )
                .replaceAll("\\n", "")
                .replace("-----END PRIVATE KEY-----", "");

        // Base64 decode the key data
        byte[] decodedPrivateKey = Base64.getDecoder().decode(privateKeyPEM);
        PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(decodedPrivateKey);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
        System.out.println(">>>>>>>>>" + privateKey);
        // Tạo đối tượng chữ ký
        Signature signature = Signature.getInstance("SHA256withRSA");

        // Khởi tạo chữ ký với private key
        signature.initSign(privateKey);

        // Cập nhật dữ liệu cần ký
        signature.update(message.getBytes());

        // Tạo chữ ký
        return signature.sign();
    }


    public static void BT2() throws Exception {
        String message = "acd";
        String privateKeyFile = "/Users/apple/huflit/network programing/code/advanced-network-programming/src/main/java/Lab/Buoi_9/Bt2/private_key_pkcs8.pem";
        String publicKeyFile = "/Volumes/SSD/huflit/network programing/code/advanced-network-programming/src/main/java/Lab/Buoi_9/Bt2/public_key.pem";

        try {
            byte[] signatureBytes = sign(privateKeyFile, message);

            // Kết nối đến server và gửi chuỗi BASE64
            Socket socket = new Socket("localhost", 5001);
            PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
            //System.out.println(Arrays.toString(signatureBytes));
            printWriter.println(Base64.getEncoder().encodeToString(signatureBytes));
            //byte[] decoded = Base64.getDecoder().decode(Base64.getEncoder().encodeToString(signatureBytes));
            //System.out.println(Arrays.toString(decoded));
            printWriter.println(message);
            printWriter.flush();

            // Đóng kết nối
            socket.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        BT2();
    }
}
```

## Server

### Bước 1: Tạo hàm xác minh chữ ký

```java
public static boolean verify(String publicKeyFile,
                             String message,
                             byte[] signatureBytes) throws Exception {

    String pemString = readFile(publicKeyFile);

    // Remove header, footer, and newline characters
    String publicKeyPEM = pemString.replace("-----BEGIN PUBLIC KEY-----", "")
            .replaceAll("\\n", "")
            .replace("-----END PUBLIC KEY-----", "");

    byte[] publicKeyPEMBytes = Base64.getDecoder().decode(publicKeyPEM);

    X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyPEMBytes);
    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

    // Tạo đối tượng chữ ký
    Signature signature = Signature.getInstance("SHA256withRSA");

    // Khởi tạo xác minh chữ ký với public key
    signature.initVerify(publicKey);

    // Cập nhật dữ liệu cần xác minh
    signature.update(message.getBytes());

    // Xác minh chữ ký
    return signature.verify(signatureBytes);
}
```

### Bước 2: Tạo hàm để nhận dữ liệu từ client sau đó chạy hàm xác minh chữ ký

```java
public static void BT2(Socket socket) throws Exception {
    inputStream = socket.getInputStream();
    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
    String signatureFromClient = reader.readLine();
    byte[] signatureFromClientDecoded = Base64.getDecoder().decode(signatureFromClient);
    //String messageFromClient = reader.readLine();
    String messageFromClient = "abc";
    System.out.println(Arrays.toString(signatureFromClient.getBytes()));
    String publicKeyFile = "/Users/apple/huflit/network programing/code/advanced-network-programming/src/main/java/Lab/Buoi_9/Bt2/public_key.pem";
    boolean isValid = verify(publicKeyFile, "abc", signatureFromClientDecoded);

    if (isValid) {
        System.out.println("Chữ ký hợp lệ");
    } else {
        System.out.println("Chữ ký không hợp lệ");
    }
}
```

### Code đầy đủ

```java
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.*;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;

public class Server {
    private static InputStream inputStream;

    public static void main(String[] args) throws Exception {
        // Tạo ServerSocket để lắng nghe kết nối
        ServerSocket serverSocket = new ServerSocket(5001);

        while (true) {
            // Chấp nhận kết nối từ client
            Socket socket = serverSocket.accept();

            BT2(socket);

            // Đóng kết nối
            socket.close();
        }
    }

    public static void BT2(Socket socket) throws Exception {
        inputStream = socket.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String signatureFromClient = reader.readLine();
        byte[] signatureFromClientDecoded = Base64.getDecoder().decode(signatureFromClient);
        //String messageFromClient = reader.readLine();
        // Nếu tin nhắn của client là "abcd thì hàm verify mới hợp lệ"
        String messageFromClient = "abcd";
        System.out.println(Arrays.toString(signatureFromClient.getBytes()));
        String publicKeyFile = "/Users/apple/huflit/network programing/code/advanced-network-programming/src/main/java/Lab/Buoi_9/Bt2/public_key.pem";
        boolean isValid = verify(publicKeyFile, "abc", signatureFromClientDecoded);

        if (isValid) {
            System.out.println("Chữ ký hợp lệ");
        } else {
            System.out.println("Chữ ký không hợp lệ");
        }
    }

    public static boolean verify(String publicKeyFile,
                                 String message,
                                 byte[] signatureBytes) throws Exception {

        String pemString = readFile(publicKeyFile);

        // Remove header, footer, and newline characters
        String publicKeyPEM = pemString.replace("-----BEGIN PUBLIC KEY-----", "")
                .replaceAll("\\n", "")
                .replace("-----END PUBLIC KEY-----", "");

        byte[] publicKeyPEMBytes = Base64.getDecoder().decode(publicKeyPEM);

        X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyPEMBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

        // Tạo đối tượng chữ ký
        Signature signature = Signature.getInstance("SHA256withRSA");

        // Khởi tạo xác minh chữ ký với public key
        signature.initVerify(publicKey);

        // Cập nhật dữ liệu cần xác minh
        signature.update(message.getBytes());

        // Xác minh chữ ký
        return signature.verify(signatureBytes);
    }

    public static String readFile(String filePath) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (FileReader fr = new FileReader(filePath)) {
            int c;
            while ((c = fr.read()) != -1) {
                sb.append((char) c);
            }
        }
        return sb.toString();
    }
}
```