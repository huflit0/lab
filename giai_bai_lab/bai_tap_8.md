# Bài tập 1

## Bước 1: tạo interface

```java
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ICalculator extends Remote {
    public int AddTwoNum(int a, int b) throws RemoteException;
}
```

## bước 2: Tạo chức năng tính cộng

* Lớp CalculatorService kế thừa lớp UnicastRemoteObject và implements interface vừa tạo (ICalculator).
* Hàm AddTwoNum trả về kết quả là tổng 2 số

```java 
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class CalculatorService extends UnicastRemoteObject implements ICalculator {

    public CalculatorService() throws RemoteException {
    }

    @Override
    public int AddTwoNum(int a, int b) throws RemoteException {
        return a + b;
    }
}
```

## Bước 3: tạo RMI Server

* ```LocateRegistry.createRegistry(9090);``` là để đăng ký port với hệ thống.
* ```Naming.bind("rmi://localhost:9090/AddRMI", iCalculator);``` khi server chạy, server sẽ public ra một URL để client
  có thể kết nối vào được.

```java
public class RMIServer {
    public static void main(String[] args) {
        try {
            LocateRegistry.createRegistry(9090);
            ICalculator iCalculator = new CalculatorService();
            Naming.bind("rmi://localhost:9090/AddRMI", iCalculator);

        } catch (IOException | AlreadyBoundException e) {
            throw new RuntimeException(e);
            e.printStackTrace();
        }
    }
}
```

## Bước 4: Tạo interface ở client giống như bước 1.

```java
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ICalculator extends Remote {
    public int AddTwoNum(int a, int b) throws RemoteException;
}
```

## Bước 5: Từ client gọi đến server.

* ```ICalculator iCalculator = (ICalculator) Naming.lookup("rmi://localhost:9090/AddRMI");``` gọi đến đầu URL server đã
  public ở bước 3.
* ```System.out.println("result: " + iCalculator.AddTwoNum(2, 2));``` gọi đến hàm thông qua interface được tạo ra ở bước
  1

```java
public class RMIClient {

    public static void main(String[] args) {
        try {
            ICalculator iCalculator = (ICalculator) Naming.lookup("rmi://localhost:9090/AddRMI");
            System.out.println("result: " + iCalculator.AddTwoNum(2, 2));
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
```
