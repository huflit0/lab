# Bài tập 3

## Client

### Bước 1: Khai báo các biến

```agsl
private static InetAddress host;
private static final int PORT = 2082;
private static DatagramSocket datagramSocket;
private static DatagramPacket inPacket, outPacket;
private static byte[] buffer;
```

### Bước 2: Tạo hàm main

* Đầu tiên sẽ kiểm tra host có tồn tại hay không.
* Nếu tồn tại host, thực hiện chạy hàm accessServer().

```agsl
public static void main(String[] args) {
    try {
        host = InetAddress.getLocalHost();
    } catch (UnknownHostException unknownHostException) {
        System.out.println("ERROR : " + unknownHostException.getMessage());
        System.exit(1);
    }
    accessServer();
}
```

### Bước 3: Tạo hàm accessServer()

* Hàm này có chức năng gửi tin nhắn dạng UDP vào server thông qua ```datagramSocket.send(outPacket);```
* ```buffer = new byte[100000];``` buffer này chỉ cho phép client nhận được 100000 byte từ server.
* ```datagramSocket.receive(inPacket);``` lệnh này có chức năng nhận tin nhắn từ server.
* Chuyển đổi tin nhắn từ byte sang chuỗi ```response = new String(inPacket.getData(), 0, inPacket.getLength());```
* Và in ra kết quả ```System.out.println("\n SERVER RESPONSE > " + response);```

```agsl
private static void accessServer() {
    try {
        datagramSocket = new DatagramSocket();              //STEP 1
        Scanner userEntry = new Scanner(System.in);
        String message = "", response = "";
        do {
            System.out.print("Nhập thông tin muốn gửi đến server: ");
            message = userEntry.nextLine();
            //if (!message.equals("***CLOSE***")) {
            outPacket = new DatagramPacket(message.getBytes(), message.length(), host, PORT); //STEP 2
            datagramSocket.send(outPacket); //STEP 3
            buffer = new byte[100000]; //STEP 5
            inPacket = new DatagramPacket(buffer, buffer.length); //STEP 5
            datagramSocket.receive(inPacket); //STEP 6
            response = new String(inPacket.getData(), 0, inPacket.getLength()); //STEP 7
            System.out.println("\n SERVER RESPONSE > " + response);
            //}

        } while (!message.equals("CLOSE"));
    } catch (IOException ioException) {
        ioException.printStackTrace();
        System.out.println("ERROR" + ioException.getMessage());

    } finally {
        System.out.println("\n * Closing connection ... *");
        datagramSocket.close();  // STEP 8

    }
}
```

## Server

### Bước 1: Khởi tạo các biến.

```agsl
private static final int PORT = 2082;
private static DatagramSocket datagramSocket;
private static DatagramPacket inPacket, outPacket;
private static byte[] buffer;
```

### Bước 2: Tạo hàm main để nhận kết nối UDP.

* Server nhận kết nối UDP thông qua port ```datagramSocket = new DatagramSocket(PORT);```
* hàm ```handleClient()``` thực thi các lệnh từ client.

```agsl
public static void main(String[] args) {

    System.out.println("Opening port. . . \n");
    try {
        datagramSocket = new DatagramSocket(PORT);              //STEP 1
    } catch (SocketException socketException) {
        System.out.println("UNABLE TO OPEN PORT");
        System.out.println(socketException.getMessage());
        System.exit(1);
    }
    handleClient();
}
```

### Bước 3: Tạo hàm handle client

* trong hàm này sẽ có các biến để

```agsl
String messageIn, messageOut;
InetAddress clientAddress = null;
int clientPort;
```

* Tạo buffer nhận message từ client

```agsl
buffer = new byte[256];         //STEP 2
inPacket = new DatagramPacket(buffer, buffer.length); //STEP 3
datagramSocket.receive(inPacket);  
```

* Lấy host và port của client

```agsl
clientAddress = inPacket.getAddress();              //STEP 5
clientPort = inPacket.getPort();
```

* Nhận tin nhắn từ client

```agsl
messageIn = new String(inPacket.getData(), 0, inPacket.getLength());
```

* Chạy hàm đếm ngược và in ra kết quả

```agsl
messageOut = "Message " + numMessages + " : " + countdown(Integer.parseInt(messageIn));
```

* Chạy hàm scan port và in ra kết quả

```agsl
messageOut = scanUDPPort(hostAndPort[0], Integer.parseInt(hostAndPort[1]));
```

* Gửi kết quả về client

```agsl
outPacket = new DatagramPacket(messageOut.getBytes(), messageOut.length(), clientAddress, clientPort); //STEP 7
datagramSocket.send(outPacket);
```

#### Tổng hợp code hàm handleClient()

```agsl
public static void handleClient() {
    try {
        String messageIn, messageOut;
        int numMessages = 0;
        InetAddress clientAddress = null;
        int clientPort;
        do {
            buffer = new byte[256];         //STEP 2
            inPacket = new DatagramPacket(buffer, buffer.length); //STEP 3
            datagramSocket.receive(inPacket);                   //STEP 4

            clientAddress = inPacket.getAddress();              //STEP 5
            clientPort = inPacket.getPort();

            messageIn = new String(inPacket.getData(), 0, inPacket.getLength());  //STEP 6
            System.out.println("Message Received");
            numMessages++;
            //bt1
            //messageOut = "Message " + numMessages + " : " + countdown(Integer.parseInt(messageIn));

            //bt2
            String[] hostAndPort = messageIn.split(" ");
            messageOut = scanUDPPort(hostAndPort[0], Integer.parseInt(hostAndPort[1]));

            outPacket = new DatagramPacket(messageOut.getBytes(), messageOut.length(), clientAddress, clientPort); //STEP 7
            datagramSocket.send(outPacket);  //STEP 8
        } while (true);

    } catch (IOException ioException) {
        ioException.printStackTrace();
    } finally {
        System.out.println("\n *CLOSING THE CONNECTION*");
        datagramSocket.close();
    }
}
```

## Bước 4: Tạo hàm countDown()

* ```for (int i = number; i >= 0; i--)``` đếm ngược số.

```agsl
private static String countDown(int number) {
    String result = "";
    for (int i = number; i >= 0; i--) {
        result += Integer.toString(i) + " ";
    }

    return result;
}
```

## Bước 5: Tạo hàm scanUDPPort()

* Lưu kết quả vào ArrayList ```List<String> result = new ArrayList<String>();```
* Hàm try/catch để thực hiện gửi gói tin UDP, nếu gửi được thì sẽ thêm kết quả thành công vào ArrayList, Không gửi được
  sẽ thêm kết quả không thành công vào ArrayList

```agsl
private static String scanUDPPort(String host, int port) throws IOException {
    List<String> result = new ArrayList<String>();

    try (DatagramSocket socket = new DatagramSocket()) {
        for (int i = 1; i <= port; i++) {
            // Create a small empty packet to send
            byte[] data = "Hello World!".getBytes();
            DatagramPacket packet = new DatagramPacket(data, data.length, InetAddress.getByName(host), i);
            packet.setAddress(InetAddress.getByName(host));
            packet.setPort(i);

            // Send the packet
            socket.send(packet);

            // Wait a short time for a response (might not be reliable)
            try {
                byte[] response = new byte[1024];
                DatagramPacket responsePacket = new DatagramPacket(response, response.length);
                socket.setSoTimeout(1000); // Set timeout in milliseconds
                socket.receive(responsePacket);
                result.add("Port " + i + " is open on " + host);
            } catch (SocketTimeoutException e) {
                result.add("Port " + i + " might be closed on " + host);
            }
        }
        return String.join("\n", result);
    }
}
```