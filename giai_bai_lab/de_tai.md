# Tên đề tài: Đảm bảo tính toàn vẹn dữ liệu bằng chữ ký số
# Các bước thực hiện

### Bước 1: thực hiện ở Client

<image src="images/buoc1.png" alt="Image description">

* Client thực hiện ký số (SHA256WithRSA).
* Sau khi ký xong, sẽ Base64 lại toàn bộ chữ ký.
* Sau khi thực hiện xong 2 bước là ký số và Base64 thì client sẽ gửi 2 thông tin đến server bằng socket:
    * Đoạn mã đã Base64.
    * Tin nhắn gốc trước khi ký số.

### Bước 2: Thực hiện ở Server

<image src="images/buoc2.png" alt="Image description">

* Lúc này Server đã nhận được hai thông tin trên của client gửi đến.
* Server sẽ thực hiện decode Base64 ra được một đoạn mã.
* Server sẽ lấy đoạn mã này kèm với tin nhắn gốc để verify chữ ký.
* Nếu đúng, Server sẽ gửi thông điệp về client ```VERIFIED;HUFLIT```

### Bước 3: Thực hiện ở Client

<image src="images/buoc3.png" alt="Image description">

* Ban đầu khi Client chạy sẽ mặc định chỉ kết nối vào server đường dẫn sau: ```http://localhost:3000/test```. Server sẽ
  mặc định mở ```http://localhost:3000/testHUFLIT```
* Sau khi nhận được thông điệp ```VERIFIED;HUFLIT``` từ server, Client sẽ cắt chuỗi này ra bằng
  hàm ```arrResponse = responseFromServer.split(';')```. Lúc này ```arrResponse``` sẽ có 2 phần tử:
    * Phần tử thứ 1 (```arrResponse[0]```) sẽ là ```VERIFIED```
    * Phần tử thứ 2 (```arrResponse[1]```) sẽ là ```HUFLIT```
* Lúc này Client sẽ nối chuỗi vào để gọi RMI ```http://localhost:3000/test + arrResponse[1]``` thì URL khi gọi qua
  server sẽ là ```http://localhost:3000/testHUFLIT```

### Bước 4: Thực hiện ở Server

* Server sẽ chạy RMI với URL là ```http://localhost:3000/testHUFLIT```.
* Server sẽ kết nối đến database để RMI truy vấn dữ liệu.

## Note

* Client khi ký dữ liệu sẽ đọc ```Private Key``` từ file (Hoặc database).
* Server sẽ đọc ```Public Key``` từ file (Hoặc database).
* upload file word vào đây: https://drive.google.com/drive/folders/1fIDPtVP18kbRLml3H8C9zA5CzgOFpsAj?usp=sharing