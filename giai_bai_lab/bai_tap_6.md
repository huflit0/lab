# Bài tập 1

## Client

### Tạo socket kết nối với server

```agsl
Socket socket = new Socket("localhost", PORT);
```

### Gửi yêu cầu in ra các số

```agsl
PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
out.println("1000");
```

### Nhận kết quả từ server

```agsl
Scanner input = new Scanner(socket.getInputStream());
while (input.hasNextLine()) {
    String line = input.nextLine();
    System.out.println(line);
}
```

### Đóng socket

```agsl
socket.close();
```

### Code đầy đủ client

```java
import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private static final int PORT = 8080;

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        // Tạo socket kết nối với server
        Socket socket = new Socket("localhost", PORT);

        // Gửi yêu cầu in ra các số
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        out.println("1000");

        // Nhận kết quả từ server
        Scanner input = new Scanner(socket.getInputStream());
        while (input.hasNextLine()) {
            String line = input.nextLine();
            System.out.println(line);
        }


        // Đóng socket
        socket.close();
    }
}
```

## Server

### Tạo socket server

```agsl
ServerSocket serverSocket = new ServerSocket(PORT);
```

### Chấp nhận kết nối từ client

```agsl
Socket socket = serverSocket.accept();
```

### Khởi chạy luồng xử lý yêu cầu

```agsl
new Thread(() -> {...}
```

### Nhận yêu tin nhắn client

```agsl
Scanner input = new Scanner(socket.getInputStream());
String line = input.nextLine();
```

### Chia dữ liệu thành hai mảng

```agsl
int[] data1 = new int[Integer.parseInt(line) + 1];
int[] data2 = new int[Integer.parseInt(line) + 1];
```

### mảng 1 chứa số chẵn, mảng 2 chứa số lẻ

```agsl
for (int i = 0; i <= Integer.parseInt(line); i++) {
    if (i % 2 == 0) {
        data1[i] = i;
    } else {
        data2[i] = i;
}
```

### Tạo hai luồng để xử lý việc in ra từng phần dữ liệu

```agsl
Thread thread1 = new Thread(() -> printData(socket, data1, 1));
Thread thread2 = new Thread(() -> printData(socket, data2, 2));
```

### Khởi chạy hai luồng

```agsl
thread1.start();
thread2.start();
```

### Chờ 2 luồng kết thúc

```agsl
thread1.join();
thread2.join();
```

### Gửi thông báo hoàn thành về client

```agsl
PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
out.println("đã in xong các số từ 1 đến: " + Integer.parseInt(line) + " thông qua 2 thread");
```

### Đóng socket

```agsl
socket.close();
```

### Hàm in dữ liệu chẵn lẻ

```agsl
private static void printData(Socket socket, int[] data, int threadNum) {
    try {
        // In ra từng số
        for (int number : data) {
            if (number != 0) { // do thread-1 các index chẵn sẽ có số, index lẽ sẽ là số 0 và ngược lại
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                out.println("Thread " + threadNum + ": " + number);
            }
        }
    } catch (IOException e) {
        e.printStackTrace();
    }
}
```

### Code đầy đủ server

```agsl
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {
    private static final int PORT = 8080;

    public static void main(String[] args) throws IOException {
        // Tạo socket server
        ServerSocket serverSocket = new ServerSocket(PORT);
        System.out.println("Server is running");
        while (true) {
            // Chấp nhận kết nối từ client
            Socket socket = serverSocket.accept();
            System.out.println("Có kết nối mới kết server: " + socket.getPort());
            // Khởi chạy luồng xử lý yêu cầu
            new Thread(() -> {
                try {
                    // Nhận yêu cầu từ client
                    Scanner input = new Scanner(socket.getInputStream());
                    String line = input.nextLine();

                    // Chia dữ liệu thành hai phần
                    int[] data1 = new int[Integer.parseInt(line) + 1];
                    int[] data2 = new int[Integer.parseInt(line) + 1];

                    for (int i = 0; i <= Integer.parseInt(line); i++) {
                        if (i % 2 == 0) {
                            data1[i] = i;
                        } else {
                            data2[i] = i;
                        }
                    }

                    // Tạo hai luồng để xử lý việc in ra từng phần dữ liệu
                    Thread thread1 = new Thread(() -> printData(socket, data1, 1));
                    Thread thread2 = new Thread(() -> printData(socket, data2, 2));

                    // Khởi chạy hai luồng
                    thread1.start();
                    thread2.start();

                    // Chờ cho hai luồng kết thúc
                    thread1.join();
                    thread2.join();

                    // Gửi thông báo hoàn thành
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                    out.println("đã in xong các số từ 1 đến: " + Integer.parseInt(line) + " thông qua 2 thread");

                    // Đóng socket
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }).start();
        }
    }

    private static void printData(Socket socket, int[] data, int threadNum) {
        try {
            // In ra từng số
            for (int number : data) {
                if (number != 0) { // do thread-1 các index chẵn sẽ có số, index lẽ sẽ là số 0 và ngược lại
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                    out.println("Thread " + threadNum + ": " + number);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
```