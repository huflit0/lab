# Bài tập 1

### Thực hiện cho nhập từ bàn phím vuh trí lưu

```agsl
Scanner scanner = new Scanner(System.in);
System.out.print("Nhập vị trí lưu trữ file: ");
String savePath = scanner.nextLine();
```

### Tạo ArrayList và đưa các URL vào

```agsl
List<String> imageUrls = new ArrayList<>();
imageUrls.add("https://letsenhance.io/static/8f5e523ee6b2479e26ecc91b9c25261e/1015f/MainAfter.jpg");
imageUrls.add("https://img.freepik.com/free-photo/painting-mountain-lake-with-mountain-background_188544-9126.jpg");
imageUrls.add("https://img.freepik.com/premium-photo/mountain-lake-with-mountain-background_901003-24960.jpg");
imageUrls.add("https://www.simplilearn.com/ice9/free_resources_article_thumb/what_is_image_Processing.jpg");
imageUrls.add("https://statusneo.com/wp-content/uploads/2023/02/MicrosoftTeams-image551ad57e01403f080a9df51975ac40b6efba82553c323a742b42b1c71c1e45f1.jpg");
imageUrls.add("https://buffer.com/cdn-cgi/image/w=1000,fit=contain,q=90,f=auto/library/content/images/size/w1200/2023/10/free-images.jpg");
```

### Tạo ExecutorService với 5 luồng

```agsl
ExecutorService executorService = Executors.newFixedThreadPool(5);
```

### Tải file

```agsl
for (String imageUrl : imageUrls) {
    executorService.submit(() -> {
        // Tải file dựa trên URL
        try {
            // Tải xuống hình ảnh
            System.out.println("Bắt đầu tải");
            URL url = new URL(imageUrl);
            readableChannel = Channels.newChannel(url.openStream());

            // Tạo tên tệp từ URL
            String fileName = imageUrl.substring(imageUrl.lastIndexOf('/') + 1);

            //Tạo tệp lưu trữ
            File file = new File(savePath + File.separator + fileName);
            try (FileOutputStream outputStream = new FileOutputStream(file)) {
                outputStream.getChannel().transferFrom(readableChannel, 0, Long.MAX_VALUE);
                // Hiển thị thông báo tải xuống
                System.out.println("Tải xong: " + file.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }
    });
}
```

### Code đầy đủ

```agsl
public class AsyncImageDownloader {
    private static final int NUM_IMAGES = 5;

    public static void main(String[] args) throws IOException {
        // Nhập vị trí lưu trữ từ người dùng
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập vị trí lưu trữ hình ảnh: ");
        String savePath = scanner.nextLine();

        // Danh sách URL hình ảnh
        List<String> imageUrls = new ArrayList<>();
        imageUrls.add("https://letsenhance.io/static/8f5e523ee6b2479e26ecc91b9c25261e/1015f/MainAfter.jpg");
        imageUrls.add("https://img.freepik.com/free-photo/painting-mountain-lake-with-mountain-background_188544-9126.jpg");
        imageUrls.add("https://img.freepik.com/premium-photo/mountain-lake-with-mountain-background_901003-24960.jpg");
        imageUrls.add("https://www.simplilearn.com/ice9/free_resources_article_thumb/what_is_image_Processing.jpg");
        imageUrls.add("https://statusneo.com/wp-content/uploads/2023/02/MicrosoftTeams-image551ad57e01403f080a9df51975ac40b6efba82553c323a742b42b1c71c1e45f1.jpg");
        imageUrls.add("https://buffer.com/cdn-cgi/image/w=1000,fit=contain,q=90,f=auto/library/content/images/size/w1200/2023/10/free-images.jpg");
        // Tạo ExecutorService với 5 luồng
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        // Tải xuống hình ảnh bất đồng bộ
        for (String imageUrl : imageUrls) {
            executorService.submit(() -> {
                // Tải file dựa trên URL
                try {
                    // Tải xuống hình ảnh
                    System.out.println("Bắt đầu tải");
                    URL url = new URL(imageUrl);
                    ReadableByteChannel readableChannel = Channels.newChannel(url.openStream());

                    // Tạo tên tệp từ URL
                    String fileName = imageUrl.substring(imageUrl.lastIndexOf('/') + 1);

                    // Tạo tệp lưu trữ
                    File file = new File(savePath + File.separator + fileName);
                    try (FileOutputStream outputStream = new FileOutputStream(file)) {
                        outputStream.getChannel().transferFrom(readableChannel, 0, Long.MAX_VALUE);
                    }

                    // Hiển thị thông báo tải xuống
                    System.out.println("Tải xong: " + file.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }

        executorService.shutdown();
        // ExecutorService tự động đóng khi tất cả các tác vụ hoàn thành
    }
}
```

# Bài tập 2

## Client

### Nhập văn bản cần tìm kiếm

```agsl
Scanner scanner = new Scanner(System.in);
System.out.print("Nhập văn bản cần tìm kiếm: ");
String searchText = scanner.nextLine();
```

### Nhập danh sách tệp văn bản

```agsl
System.out.print("Nhập danh sách tệp văn bản (cách nhau bởi dấu phẩy): ");
String fileNames = scanner.nextLine();
List<String> fileNamesList = new ArrayList<>();
for (String fileName : fileNames.split(",")) {
    fileNamesList.add(fileName.trim());
}
```

### Tạo socket kết nối với server

```Socket socket = new Socket(HOST, PORT);```

### Gửi thông tin tìm kiếm đến server

```agsl
ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
oos.writeObject(searchText);
oos.writeObject(fileNamesList);
oos.flush();
```

### Nhận kết quả tìm kiếm từ server

```agsl
for (String fileName : fileNamesList) {
    ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
    int count = (int) ois.readObject();

    // Hiển thị kết quả
    System.out.println("Tệp: " + fileName);
    System.out.println("Số lần xuất hiện: " + count);
}
```

### Đóng socket

```agsl
socket.close();
```

### Code đầy đủ client

```java
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Client {
    private static final int PORT = 8080;
    private static final String HOST = "localhost";

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        // Nhập văn bản cần tìm kiếm
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập văn bản cần tìm kiếm: ");
        String searchText = scanner.nextLine();

        // Nhập danh sách tệp văn bản
        System.out.print("Nhập danh sách tệp văn bản (cách nhau bởi dấu phẩy): ");
        String fileNames = scanner.nextLine();
        List<String> fileNamesList = new ArrayList<>();
        for (String fileName : fileNames.split(",")) {
            fileNamesList.add(fileName.trim());
        }

        // Tạo socket kết nối với server
        Socket socket = new Socket(HOST, PORT);

        // Gửi thông tin tìm kiếm đến server
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
        oos.writeObject(searchText);
        oos.writeObject(fileNamesList);
        oos.flush();

        // Nhận kết quả tìm kiếm từ server
        for (String fileName : fileNamesList) {
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            int count = (int) ois.readObject();

            // Hiển thị kết quả
            System.out.println("Tệp: " + fileName);
            System.out.println("Số lần xuất hiện: " + count);
        }

        // Đóng socket
        socket.close();
    }
}
```

## Server

### Tạo socket server

```agsl
ServerSocket serverSocket = new ServerSocket(PORT))
```

### Chấp nhận kết nối từ client

```agsl
Socket socket = serverSocket.accept();
```

### Nhận văn bản cần tìm kiếm và danh sách tệp văn bản từ client

```agsl
ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
String searchText = (String) ois.readObject();
List<String> files = (List<String>) ois.readObject();
```

### Tìm kiếm văn bản trong các tệp và gửi kết quả cho client

```agsl
for (String file : files) {
    int count = countOccurrences(file, searchText);
    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
    oos.writeObject(count);
    oos.flush();
}
```

### Đóng socket

```socket.close();```

### Hàm đếm số lượng số lần xuất hiện chuỗi ký tự trong file

```java
private static int countOccurrences(String file, String searchText) throws IOException {
    AtomicInteger count = new AtomicInteger(0);
    Files.lines(new File(file).toPath()).forEach(line -> {
        if (line.contains(searchText)) {
            count.incrementAndGet();
        }
    });
    return count.get();
}
```

### Code đầy đủ server

```java
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Server {
    private static final int PORT = 8080;

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        try (// Tạo socket server

             ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("server is running");
            while (true) {
                // Chấp nhận kết nối từ client
                Socket socket = serverSocket.accept();

                // Nhận văn bản cần tìm kiếm và danh sách tệp văn bản từ client
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                String searchText = (String) ois.readObject();
                List<String> files = (List<String>) ois.readObject();

                // Tìm kiếm văn bản trong các tệp và gửi kết quả cho client
                for (String file : files) {
                    int count = countOccurrences(file, searchText);
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject(count);
                    oos.flush();
                }

                // Đóng socket
                socket.close();
            }
        }
    }

    private static int countOccurrences(String file, String searchText) throws IOException {
        AtomicInteger count = new AtomicInteger(0);
        Files.lines(new File(file).toPath()).forEach(line -> {
            if (line.contains(searchText)) {
                count.incrementAndGet();
            }
        });
        return count.get();
    }
}
```