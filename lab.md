# Buổi 1

## Bài tập 1

Viết một lớp Student để biểu diễn một học sinh. Lớp Student cần có các thuộc tính:

- name: Tên của học sinh.
- age: Tuổi của học sinh.
- scores: Một mảng lưu trữ điểm của học sinh cho các môn học.

Viết các phương thức cho lớp Student:

- constructor(name, age): Khởi tạo giá trị cho các thuộc tính name, age.
- getName(): Lấy giá trị của thuộc tính name.
- getAge(): Lấy giá trị của thuộc tính age.
- getScores(): Lấy giá trị của thuộc tính scores.
- setName(name): Cập nhật giá trị của thuộc tính name.
- setAge(age): Cập nhật giá trị của thuộc tính age.
- addScore(score): Thêm điểm mới vào mảng scores khi nào nhấn ‘-1’ thì dừng.
- calculateAverageScore(): Tính điểm trung bình của tất cả các môn học.
- toString(): Trả về chuỗi biểu diễn thông tin của học sinh.

## Bài tập 2

Sử dụng lớp InetAddress để lấy địa chỉ ip từ 1 domain.

## Bài tập 3:

Viết một lớp CheckSubdomain với các thuộc tính:

- domain: tên domain muốn dùng
- subdomains: tên file chứa các subdomain

Viết các phương thức cho lớp CheckSubdomain:

- Construcor: giá trị mặc định của domain là google.com
- setDomain(): cập nhật giá trị domain
- setSubdomain(): cập nhập giá trị subdomains
- check(): Sử dụng lớp InetAddress và file subdomain được cung cấp để brute-force. Nếu host còn hoạt động thì ghi host
  và ip ghi vào file.

# Buổi 2

## Bài tập 1:

Viết chương trình Client-Server:

- Client tạo menu: 1. Kiểm tra số nguyên tố, 2. Gửi file, 3. Exit
- Client gửi một số đến server, server kiểm tra nếu là số nguyên tố thì gửi một response về client là “[số đã gửi] là số
  nguyên tố”, nếu không phải “[số đã gửi] không phải là số nguyên tố”

## Bài tập 2:

Thực hiện menu số 2 là gửi file từ client đến server.

# Buổi 3

## Bài tập 1:

Viết chương trình Client-Server:

- Client gửi một số đến server, server trả về dãy số đếm ngược từ số đó. (Lưu ý, phía client phải cho nhập liên tục khi
  nào client gửi message là “CLOSE” thì sẽ đóng kết nối).

## Bài tập 2:

Viết chương trình Client-Server:

- Client gửi host và port (server sẽ scan từ 1 đến port này) với cấu trúc như sau: [host][space][range_port]
- Server nhận được message từ client thì tiến hành scan range udp port đó có mở hay không và trả về client kết quả.

# Buổi 4: Tạo menu với 3 lựa chọn: Download file, Download html code, Whois và Exit.

## Bài tập 1:

Tạo lớp HttpDownloader sử dụng lớp java.net.URL, java.net.HttpURLConnection để download file.
Cho nhập Url file cần tải và lưu file vào thư mục tự chọn.

## Bài tập 2:

- Sử dụng các lớp như java.net.URL và java.net.URLConnection viết chương trình cho người dùng nhập 1 giá trị là host.
- Chức năng chương trình sẽ lấy toàn bộ html của host.

## Bài tập 3:

- Kết nối Socket với whois qua thông tin sau và lấy thông tin của domain khi người dùng nhập vào:
- Host: whois.internic.com
- Port: 43

# Buổi 5

## Bài tập 1:

Sử dụng ExecutorService để thực hiện tải 5 file cùng 1 lúc, chứng minh đang chạy đa luồng.

## Bài tập 2:

Xây dựng mô hình Client-Server:

- Cho người dùng nhập vào chuỗi ký tự.
- Cho người dùng nhập vào danh sách các file
  Kiểm tra chuỗi ký tự đó nếu có trong file nào thì in ra tên file và số lần xuất hiện của chuỗi ký tự.

# Buổi 6

## Bài tập 1:  Viết chương trình Client-Server, cho nhập vào 1 số X và in ra các số từ 1 đến X với yêu cầu sau:

- Tạo 2 Thread, Thread-1 in ra số chẵn, Thread-2 in ra số lẻ.

## Bài tập 2: Viết chương trình Client-Server thực hiện upload file:

- Tạo lớp Client với các thuộc tính và hàm như sau:  
  -- serverAddress: ip của server
  -- serverPort: port socket
  -- filePath: đường dẫn file cần upload
  -- Constructor để nhận các thuộc tính trên
  -- Hàm upload File()

- Tạo lớp UploadThread và override lại hàm run() để tạo luồng mới, gửi toàn bộ dữ liệu của file lên server.
- Tạo lớp Server với các thuộc tính và hàm như sau:
  -- serverPort: port của socket
  -- handleClientRequest(): hàm nhận request từ client
  -- receiveFile(): hàm nhận thông tin file từ lớp UploadThread và lưu file
  -- start(): hàm chạy server

Tạo lớp MainServer để chạy server, MainClient để chạy client

# Buổi 7

- Bài tập: Viết chương trình Client-Server chat nhóm.
- Làm các bài chưa hoàn thành.

# Buổi 8

## Bài tập 1:

- Sử dụng RMI tính tổng 2 số.
- Sử dụng RMI hiển thị toàn bộ file trong thư mục.

## Bài tập 2 (optional):

- Client kết nối đến server Rabbit MQ và luôn luôn lắng nghe.
- Server gửi message lên Rabbit, sau đó client nhận được message từ Rabbit và thực hiện in ra dòng chữ "đã nhận
  được <message>"

# Buổi 9

## Bài tập 1: Viết chương trình Client-Server:

- Client thực hiện hash SHA256(message) và BASE64(SHA256(message)) gửi lên server.
- Server nhận được message và in lại chuỗi SHA256(message).

## Bài tập 2: Viết chương trình Client-Server thực hiện ký số và xác thực message:

- Sinh ra cặp key RSA-2048. (private_key dùng để ký, public_key dùng verify)
- Client gửi một message với mã hoá BASE64(SHA256withRSA) kèm theo message lên server.
- Server sẽ xác thực lại xem message đó có đúng là message đã được ký không.