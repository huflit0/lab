package Lab.Buoi_1;

import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CheckSubdomain {
    private String domain;
    private String subdomains;

    public CheckSubdomain() {
        this.domain = "google.com";
        this.subdomains = "";
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public void setSubdomains(String subdomains) {
        this.subdomains = subdomains;
    }

    public void check() {
        try {
            List<String> allLines = Files.readAllLines(Paths.get(this.subdomains));

            try (FileWriter myWriter = new FileWriter("result.txt")) {

                for (String line : allLines) {
                    try {
                        InetAddress address = InetAddress.getByName(line + "." + this.domain);
                        System.out.println(address + " => tồn tại");
                        myWriter.write(address.toString() + "\n");
                    } catch (IOException e) {
                        System.out.println(line + "." + this.domain + " không tồn tại");
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("File không tồn tại");
        }
    }

}
