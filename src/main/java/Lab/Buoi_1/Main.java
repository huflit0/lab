package Lab.Buoi_1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void baiTap1() {
        Student student = new Student("Nguyễn Văn A", 18);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập điểm: ");


        while (true) {
            int score = scanner.nextInt();
            if (score < 0) {
                break;
            }
            System.out.println("Bạn đã nhập: " + score);
            student.addScore(score);
        }
        System.out.println("\nSau khi thêm điểm:");
        System.out.println(student);
    }

    public static void baiTap2() {
        CheckSubdomain checkSubdomain = new CheckSubdomain();
        checkSubdomain.setDomain("vnexpress.net");
        checkSubdomain.setSubdomains("subdomain.txt");
        checkSubdomain.check();
    }

    public static void main(String[] args) {
//        baiTap1();
        baiTap2();
    }
}
