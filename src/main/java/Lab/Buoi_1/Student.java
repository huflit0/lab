package Lab.Buoi_1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Student {

    private String name;
    private int age;
    private List<Double> scores = new ArrayList<>();

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public ArrayList<Double> getScores() {
        // Return a copy of the list to avoid modifying the original list
        return new ArrayList<>(scores);
    }

    public void addScore(double score) {
        scores.add(score);
    }

    public double calculateAverageScore() {
        double sum = 0;
        for (double score : scores) {
            sum += score;
        }
        return sum / scores.size();
    }

    @Override
    public String toString() {
        return "Học sinh: " + name + "\n" +
                "Tuổi: " + age + "\n" +
                "Điểm: " + scores + "\n" +
                "Điểm trung bình: " + calculateAverageScore();
    }

}