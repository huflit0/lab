package Lab.Buoi_6.bt1;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client2 {
    private static final int PORT = 8080;

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        // Tạo socket kết nối với server
        Socket socket = new Socket("localhost", PORT);

        // Gửi yêu cầu in ra các số
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        out.println("1000");

        // Nhận kết quả từ server
        Scanner input = new Scanner(socket.getInputStream());
        while (input.hasNextLine()) {
            String line = input.nextLine();
            System.out.println(line);
        }


        // Đóng socket
        socket.close();
    }
}
