package Lab.Buoi_6.bt2;

public class MainClient {

	public static void main(String[] args) {

		String serverAddress = "localhost";
		int serverPort = 8000;
		String filePath = "test.txt";

		Client client = new Client(serverAddress, serverPort, filePath, 1);
		client.uploadFile();
		
		Client client2 = new Client(serverAddress, serverPort, filePath, 2);
		client2.uploadFile();

		Client client3 = new Client(serverAddress, serverPort, filePath, 3);
		client3.uploadFile();
		

	}

}
