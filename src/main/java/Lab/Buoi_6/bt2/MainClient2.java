package Lab.Buoi_6.bt2;

public class MainClient2 {

	public static void main(String[] args) {
		String serverAddress = "localhost";
		int serverPort = 8000;
		String filePath = "test.txt";
		
		Client client4 = new Client(serverAddress, serverPort, filePath, 4);
		client4.uploadFile();
		
		Client client5 = new Client(serverAddress, serverPort, filePath, 5);
		client5.uploadFile();
		
		Client client6 = new Client(serverAddress, serverPort, filePath, 6);
		client6.uploadFile();

	}

}
