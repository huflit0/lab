package Lab.Buoi_6.bt2;

import java.io.*;
import java.net.Socket;


public class Client {
	private String serverAddress;
    private int serverPort;
    private String filePath;
    private int clientNum;
    
    public Client(String serverAddress, int serverPort, String filePath, int clientNum) {
        this.serverAddress = serverAddress;
        this.serverPort = serverPort;
        this.filePath = filePath;
        this.clientNum = clientNum;
    }

    public void uploadFile() {
    	
        try (Socket socket = new Socket(serverAddress, serverPort)) {
            // Tạo luồng để upload file
            Thread uploadThread = new UploadThread(socket, filePath, clientNum);
            uploadThread.start();

            // Chờ cho đến khi luồng upload file hoàn thành
            uploadThread.join();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    
}
