package Lab.Buoi_6.bt2;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	private int serverPort;

    public Server(int serverPort) {
        this.serverPort = serverPort;
    }

    public void start() {
        try (ServerSocket serverSocket = new ServerSocket(serverPort)) {
            while (true) {
                Socket socket = serverSocket.accept();

                // Xử lý yêu cầu từ Client
                new Thread(() -> handleClientRequest(socket)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleClientRequest(Socket socket) {
        try (DataInputStream dis = new DataInputStream(socket.getInputStream());
             FileOutputStream fos = new FileOutputStream("received_file.txt")) {
            String command = dis.readUTF();
            String filePath = dis.readUTF();
            long fileSize = dis.readLong();

            if (command.equals("UPLOAD_FILE")) {
                receiveFile(socket, filePath, fileSize, fos);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void receiveFile(Socket socket, String filePath, long fileSize, FileOutputStream fos) {
        try (DataInputStream dis = new DataInputStream(socket.getInputStream())) {
            byte[] buffer = new byte[1024];
            long bytesReceived = 0;
            while (bytesReceived < fileSize) {
                int bytesRead = dis.read(buffer);
                fos.write(buffer, 0, bytesRead);
                bytesReceived += bytesRead;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}