package Lab.Buoi_6.bt2;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Socket;

class UploadThread extends Thread {

	private Socket socket;
	private String filePath;
	private int clientNum;

	public UploadThread(Socket socket, String filePath, int clientNum) {
		this.socket = socket;
		this.filePath = filePath;
		this.clientNum = clientNum;
	}

	@Override
	public void run() {
		if (clientNum == 3) {
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
		System.out.println("Client " + clientNum);
		try (FileInputStream fis = new FileInputStream(filePath);
				DataOutputStream dos = new DataOutputStream(socket.getOutputStream())) {
			// Gửi thông tin file cho Server
			dos.writeUTF("UPLOAD_FILE");
			dos.writeUTF(filePath);
			long fileSize = fis.getChannel().size();
			dos.writeLong(fileSize);

			// Upload file theo từng phần
			byte[] buffer = new byte[1024];
			int bytesRead;
			while ((bytesRead = fis.read(buffer)) != -1) {
				dos.write(buffer, 0, bytesRead);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
