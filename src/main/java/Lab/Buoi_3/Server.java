package Lab.Buoi_3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class Server {
	private static final int PORT  = 2082;
    private static DatagramSocket datagramSocket;
    private static DatagramPacket inPacket,outPacket;
    private static byte[] buffer;


    public static void main(String[] args) {

        System.out.println("Opening port. . . \n");
        try {
            datagramSocket = new DatagramSocket(PORT);              //STEP 1
        } catch (SocketException socketException) {
            System.out.println("UNABLE TO OPEN PORT");
            System.out.println(socketException.getMessage());
            System.exit(1);
        }
        handleClient();
    }
    
    private static String countdown(int number) {
    	String result = "";
    	for (int i = number; i >= 0; i--) {
    	    result += Integer.toString(i) + " ";
    	}
    	
    	return result;
    }
    
    private static String scanUDPPort(String host, int port) throws IOException {
    	List<String> result = new ArrayList<String>();
    	
    	try (DatagramSocket socket = new DatagramSocket()) {
            


            for (int i = 1; i <= port; i++) {
            	// Create a small empty packet to send
            	byte[] data = "Hello World!".getBytes();
                DatagramPacket packet = new DatagramPacket(data, data.length, InetAddress.getByName(host), i);
                packet.setAddress(InetAddress.getByName(host));
                packet.setPort(i);

                // Send the packet
                socket.send(packet);

                // Wait a short time for a response (might not be reliable)
                try {
                	byte[] response = new byte[1024];
                    DatagramPacket responsePacket = new DatagramPacket(response, response.length);
                    socket.setSoTimeout(1000); // Set timeout in milliseconds
                    socket.receive(responsePacket);
                    result.add("Port " + i + " is open on " + host);
                } catch (SocketTimeoutException e) {
                	result.add("Port " + i + " might be closed on " + host);
                }
            }
            return String.join("\n", result);
        }
    	
    	
    }
    
    public static void handleClient() {
        try {
            String messageIn, messageOut;
            int numMessages = 0;
            InetAddress clientAddress = null;
            int clientPort;
            do {
                buffer = new byte[256];         //STEP 2
                inPacket = new DatagramPacket(buffer, buffer.length); //STEP 3
                datagramSocket.receive(inPacket);                   //STEP 4

                clientAddress = inPacket.getAddress();              //STEP 5
                clientPort = inPacket.getPort();
                
                messageIn = new String(inPacket.getData(), 0, inPacket.getLength());  //STEP 6
                System.out.println("Message Received");
                numMessages++;
                //bt1
//                messageOut = "Message " + numMessages + " : " + countdown(Integer.parseInt(messageIn));
                
                //bt2
                String[] hostAndPort = messageIn.split(" ");
                messageOut = scanUDPPort(hostAndPort[0], Integer.parseInt(hostAndPort[1]));
                
                
                
                outPacket = new DatagramPacket(messageOut.getBytes(), messageOut.length(), clientAddress, clientPort); //STEP 7
                datagramSocket.send(outPacket);  //STEP 8
            } while (true);

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        finally {
            System.out.println("\n *CLOSING THE CONNECTION*");
            datagramSocket.close();
            
        }
    }
}
