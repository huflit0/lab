package Lab.Buoi_2;

import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.io.*;

public class Client {

	private static InetAddress host;
	private static final int PORT = 1234;
	private static Socket link = null;
	private static Scanner input;
	private static PrintWriter output;
	private static DataOutputStream dataOutputStream = null;
	private static DataInputStream dataInputStream = null;

	public static void main(String[] args) throws Exception {

		try {
			host = InetAddress.getLocalHost();
		} catch (UnknownHostException unknownHostException) {
			System.out.println("Error caused" + unknownHostException.getMessage());
			System.exit(1);

		}
		accessServer();
	}

	// sendFile function define here
	private static void sendFile(String path) throws Exception {
		int bytes = 0;
		// Open the File where he located in your pc
		File file = new File(path);
		FileInputStream fileInputStream = new FileInputStream(file);
		
		// Here we send the File to Server
		dataOutputStream.writeLong(file.length());
		// Here we break file into chunks
		byte[] buffer = new byte[4 * 1024];
		while ((bytes = fileInputStream.read(buffer)) != -1) {
			// Send the file to Server Socket
			dataOutputStream.write(buffer, 0, bytes);
			dataOutputStream.flush();
		}
		// close the file here
		fileInputStream.close();
	}

	private static void sendFileNew(String filePath, Socket clientSocket) throws FileNotFoundException, IOException {
		File file = new File(filePath);
		if (!file.exists()) {
			System.out.println("Error: File not found: " + filePath);
			System.exit(1);
		}

		try (FileInputStream fis = new FileInputStream(file);
				Scanner reader = new Scanner(fis);
				PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true) // Autoflush
		) {
			// Send file name to server
			out.println(file.getName());

			
			while (reader.hasNextLine()) {
				String line = reader.nextLine();
				out.println(line); // Send each line of the file
			}

//			// (Optional) Receive acknowledgment from server for each line
//			String response;
//			while ((response = input.nextLine()) != null) {
//				if (response.equals("ACK")) {
//					// Acknowledgment received, continue sending
//				} else {
//					System.err.println("Error receiving confirmation: " + response);
//					break;
//				}
//			}
			System.out.println("File sent successfully!");
		}
		
		
		output.println("Done");
	}

	private static void accessServer() throws Exception {

		try {
			// Bước 1: kết nối đến server
			link = new Socket(host, PORT);

			// Bước 2: nhập input
			Scanner scanner = new Scanner(System.in);
			int choice;

			input = new Scanner(link.getInputStream());
			output = new PrintWriter(link.getOutputStream(), true);

			Scanner userEntry = new Scanner(System.in);
			String message, response;
			do {
				System.out.println("\nMenu:");
				System.out.println("1. Kiểm tra số nguyên tố");
				System.out.println("2. Gửi file");
				System.out.println("3. Exit");
				System.out.print("Enter your choice: ");
				choice = scanner.nextInt();
				output.println(choice);
				switch (choice) {
				case 1:
					System.out.print("Nhập số nguyên tố: ");
					message = userEntry.nextLine();
					output.println(message);
					response = input.nextLine();
					System.out.println("\nSERVER > " + response);
					break;
				case 2:
					Scanner scan = new Scanner(System.in);
					System.out.print("Nhập đường dẫn file: ");
					String filePath = scan.next();
					sendFileNew(filePath, link);
					
					break;
				case 3:
					message = "CLOSE";
					output.println(message);
					break;
				default:
					System.out.println("Invalid choice. Please try again.");
				}
			
				
			} while (choice != 3);

		} catch (IOException ioException) {
			ioException.printStackTrace();
		} finally {
			try {
				System.out.println("CLOSING CONNECTION...");
				// Bước 4: đóng kết nối
				link.close();
			} catch (IOException ioException) {
				System.out.println("Unable to Disconnect ERROR" + ioException.getMessage());
				System.exit(1);
			}
		}

	}

}
