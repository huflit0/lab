package Lab.Buoi_2;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {

	private static ServerSocket serverSocket;
	private static final int PORT = 1234;
	private static Socket link = null;
	private static Scanner input;
	private static PrintWriter output;
	private static DataOutputStream dataOutputStream = null;
	private static DataInputStream dataInputStream = null;

	public static void main(String[] args) throws Exception {

		System.out.println("OPENING PORT : " + PORT + "\n");
		while (true) {
			try {
				// Bước 1: server mở port để lắng nghe
				serverSocket = new ServerSocket(PORT);
			} catch (IOException ioException) {
				System.out.println("UNABLE TO ATTACH TO PORT ! EXCEPTION : " + ioException.getMessage());
				System.exit(1);
			}
			do {
				// Các bước 2->5
				handleClient();
			} while (true);
		}
	}

	public static boolean isPrime(int num) {
		if (num <= 1) {
			return false;
		}
		if (num <= 3) {
			return true;
		}

		int sqrt = (int) Math.sqrt(num);
		for (int i = 2; i <= sqrt; i++) {
			if (num % i == 0) {
				return false;
			}
		}
		return true;
	}

	private static void receiveFile(String fileName) throws Exception {
		int bytes = 0;
		FileOutputStream fileOutputStream = new FileOutputStream(fileName);
		long size = dataInputStream.readLong(); // read file size

		byte[] buffer = new byte[4 * 1024];
		while (size > 0 && (bytes = dataInputStream.read(buffer, 0, (int) Math.min(buffer.length, size))) != -1) {
			// Here we write the file using write method
			fileOutputStream.write(buffer, 0, bytes);
			size -= bytes; // read upto file size
		}
		// Here we received file
		System.out.println("File is Received");
		fileOutputStream.close();
	}

	private static void receiveFileNew(String fileName) throws Exception {

		try {

			input = new Scanner(link.getInputStream());
			output = new PrintWriter(link.getOutputStream(), true); // Autoflush

			// Create a new file on the server side
			File file = new File(fileName);
			FileOutputStream fos = new FileOutputStream(file);

			System.out.println("Receiving file: " + fileName);

			while (input.hasNextLine()) {
				String line = input.nextLine();
				output.println("ACK"); // Send acknowledgment for each line (optional)
				fos.write(line.getBytes()); // Write line content to file
			}
			System.out.println("File received successfully!");

		} catch (IOException e) {
	        System.err.println("Error receiving or saving file: " + e.getMessage());
	        output.println("error"); // Send error message to client (optional)
	    }
	}

	private static void handleClient() throws Exception {

		try {
			// Bước 2: chấp nhận kết nối từ client
			link = serverSocket.accept();

			// bước 3: lấy input từ client và ghi ra ngoài
			input = new Scanner(link.getInputStream());
			output = new PrintWriter(link.getOutputStream(), true);

			int numMessages = 0;
			// Bước 4: Lấy thông tin message từ client
			String message = input.nextLine();
			String[] arrMessage = message.split(";");
			while (true) {

				System.out.println("MESSAGE RECEIVED.");
				numMessages++;
				switch (message) {
				case "1":
					// Đoc message tiếp theo
					message = input.nextLine();
					// Kiểm tra số nguyên tố và gửi message đến client
					if (isPrime(Integer.parseInt(message))) {
						// Gửi message đến client
						output.println("MESSAGE NUM " + numMessages + " : " + message + " là số nguyên tố");

					} else {
						output.println("MESSAGE NUM " + numMessages + " : " + message + " không phải là số nguyên tố");
					}
					break;
				case "2":

					// Receive file name from client

					// String[] filePath = arrMessage[1].split("/");
					// String fileName = filePath[filePath.length - 1];
					// File file = new File(message);
					// Lấy file name từ client
					String fileName = input.nextLine();

					receiveFileNew(fileName);

					break;
				case "CLOSE":
					break;
				default:
					System.out.println("Invalid choice. Please try again.");
				}

				if (input.hasNextLine()) {
			        message = input.nextLine();
			    } else {
			        // Handle end-of-input here (e.g., break or prompt user)
			        System.err.println("Reached end of input. Connection closed?");
			        break; 
			    }	
			}
			output.println(numMessages + " messages received");

		} catch (IOException ioException) {
			ioException.printStackTrace();
			System.out.println(ioException.getMessage());

		} finally {

			try {
				System.out.println("\n* CLOSING CONNECTION . . . *");
				// Bước 5: đóng kết nối
				link.close();

			} catch (IOException e) {
				System.out.println("UNABLE TO DISCONNECT ! : " + e.getMessage());
				System.exit(1);
			}
		}
	}

}
