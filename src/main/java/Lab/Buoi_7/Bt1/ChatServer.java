package Lab.Buoi_7.Bt1;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;

public class ChatServer {

	private static List<Socket> clients = new ArrayList<>();
	private static boolean isAcceptingConnections = true;

	public static void main(String[] args) throws IOException {
		// Khởi tạo ServerSocket
		ServerSocket serverSocket = new ServerSocket(8000);

		while (true) {

			// Chấp nhận kết nối từ Client
			Socket socket = serverSocket.accept();
			clients.add(socket);

			// Khởi tạo luồng riêng cho Client
			new Thread(() -> {
				try {
					// Nhận tên người dùng
					InputStream inputStream = socket.getInputStream();
					BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
					String username = reader.readLine();

					// Gửi thông báo chào mừng đến Client
					OutputStream outputStream = socket.getOutputStream();
					PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputStream));
					writer.println("Welcome to the chat group, " + username + "!");
					writer.flush();

					// Xử lý tin nhắn từ Client
					String message;
					while ((message = reader.readLine()) != null) {
						// Gửi tin nhắn đến tất cả Client
						System.out.println(">>>>>test: " + message);
						for (Socket client : clients) {
							if (client != socket) {
								writer = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));
								writer.println("[" + username + "]: " + message);
								writer.flush();
							}
						}
					}

//					// Xóa Client khỏi danh sách
//					clients.remove(socket);
//
//					// Gửi thông báo Client đã rời khỏi nhóm
//					for (Socket client : clients) {
//						writer = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));
//						writer.println(username + " has left the chat group.");
//						writer.flush();
//					}
//
//					// Đóng kết nối
//					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}).start();

		}
	}

}
