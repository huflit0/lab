package Lab.Buoi_7.Bt1;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ChatClient {
	private static Scanner scan;
	public static void main(String[] args) throws IOException {
        // Nhập tên người dùng
		scan = new Scanner(System.in);
        System.out.print("Nhập tên người dùng: ");
        String username = scan.nextLine();

        // Khởi tạo Socket
        Socket socket = new Socket("localhost", 8000);

        // Gửi tên người dùng cho Server
        OutputStream outputStream = socket.getOutputStream();
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputStream));
        writer.println(username);
        writer.flush();

        // Nhận thông báo chào mừng từ Server
        InputStream inputStream = socket.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String message = reader.readLine();
        System.out.println(message);

        
     // Thread to receive messages from server
        Thread receiveThread = new Thread(() -> {
            try {
                while (true) {
                	BufferedReader readerMessageFromClients = new BufferedReader(new InputStreamReader(inputStream));
                    String messageFromClients = readerMessageFromClients.readLine();
                    if (messageFromClients != null) {
                        System.out.println(messageFromClients);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        receiveThread.start();
        // Gửi tin nhắn
        while (true) {
        	System.out.print("Nhập tin nhắn (" + username + "): ");
            String messageFromClient = scan.nextLine();

            // Gửi tin nhắn cho Server
            writer.println(messageFromClient);
            writer.flush();
            
        }

    }

}
