//package Lab.Buoi_7.Bt2;
//
//import java.io.*;
//import java.net.*;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//
//public class Server {
//	private static final int CHUNK_SIZE = 4096;
//
//    public static void main(String[] args) throws IOException {
//        // Cấu hình địa chỉ và port multicast
//        InetAddress multicastAddress = InetAddress.getByName("224.0.0.1");
//        int port = 5432;
//
//        // Tạo MulticastSocket
//        MulticastSocket socket = new MulticastSocket(port);
//
//        // Tham gia nhóm multicast
//        socket.joinGroup(multicastAddress);
//
//        // Chọn tệp tin để truyền tải
//        File file = new File("test.txt");
//
//        // Tạo ExecutorService để xử lý gửi dữ liệu
//        ExecutorService executorService = Executors.newSingleThreadExecutor();
//
//        try (
//            FileInputStream inputStream = new FileInputStream(file);
//            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
//        ) {
//            byte[] data = new byte[CHUNK_SIZE];
//            int bytesRead;
//
//            while ((bytesRead = bufferedInputStream.read(data)) != -1) {
//                // Gửi dữ liệu theo từng chunk
//                executorService.submit(() -> {
//                    try {
//                        DatagramPacket packet = new DatagramPacket(data, bytesRead, multicastAddress, port);
//                        socket.send(packet);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                });
//            }
//        }
//
//        // Thoát khỏi nhóm multicast
//        socket.leaveGroup(multicastAddress);
//
//        // Đóng MulticastSocket
//        socket.close();
//
//        // Đợi cho tất cả dữ liệu được gửi
//        executorService.shutdown();
//        while (!executorService.isTerminated()) {
//            try {
//                Thread.sleep(100);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//}
