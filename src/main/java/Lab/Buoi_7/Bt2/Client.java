//package Lab.Buoi_7.Bt2;
//
//import java.io.*;
//import java.net.*;
//
//public class Client {
//	private static final int CHUNK_SIZE = 4096;
//
//    public static void main(String[] args) throws IOException {
//        // Cấu hình địa chỉ và port multicast
//        InetAddress multicastAddress = InetAddress.getByName("224.0.0.1");
//        int port = 5432;
//
//        // Tạo MulticastSocket
//        MulticastSocket socket = new MulticastSocket(port);
//
//        // Tham gia nhóm multicast
//        socket.joinGroup(multicastAddress);
//
//        // Tạo thư mục lưu trữ tệp tin
//        File directory = new File("received_files");
//        directory.mkdirs();
//
//        // Tên tệp tin
//        String fileName = "received_file.txt";
//
//        // Tạo file
//        File file = new File(directory, fileName);
//        try (
//            FileOutputStream outputStream = new FileOutputStream(file);
//            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
//        ) {
//            byte[] data = new byte[CHUNK_SIZE];
//            DatagramPacket packet;
//
//            // Nhận các chunk dữ liệu
//            while ((packet = socket.receive(data)) != null) {
//                // Ghi dữ liệu vào file
//                bufferedOutputStream.write(data, 0, packet.getLength());
//            }
//        }
//
//        // Thoát khỏi nhóm multicast
//        socket.leaveGroup(multicastAddress);
//
//        // Đóng MulticastSocket
//        socket.close();
//
//        System.out.println("Tệp tin " + fileName + " đã được nhận thành công!");
//    }
//
//}
