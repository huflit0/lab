package Lab.Buoi_9.Bt2;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.Scanner;

public class Client {


    public static void BT1() throws IOException, NoSuchAlgorithmException {
        // Nhập thông điệp từ người dùng
        System.out.print("Nhập thông điệp: ");
        Scanner scan = new Scanner(System.in);
        String message = scan.nextLine();
        // Mã hóa thông điệp bằng SHA-256
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        byte[] digest = messageDigest.digest(message.getBytes());

        // In ra chuỗi sau khi SHA-256
        System.out.print("Chuỗi sau khi SHA-256: ");
        StringBuilder sb = new StringBuilder();
        for (byte b : digest) {
            sb.append(String.format("%02x", b));
        }
        System.out.println(sb.toString());

        // Chuyển đổi sang Base64
        String base64Encoded = Base64.getEncoder().encodeToString(digest);

        // In ra chuỗi sau khi Base64
        System.out.println("Chuỗi sau khi Base64: " + base64Encoded);

        // Kết nối đến server và gửi chuỗi BASE64
        Socket socket = new Socket("localhost", 5000);
        PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
        printWriter.println(base64Encoded);
        System.out.print(digest);
        printWriter.flush();

        // Đóng kết nối
        socket.close();
    }

    public static String readFile(String filePath) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (FileReader fr = new FileReader(filePath)) {
            int c;
            while ((c = fr.read()) != -1) {
                sb.append((char) c);
            }
        }
        return sb.toString();
    }

    public static byte[] sign(String privateKeyFile, String message) throws Exception {
        //openssl genrsa -out private.pem 1024
        //openssl rsa -in private.pem -pubout -outform PEM -out public_key.pem
        //openssl pkcs8 -topk8 -inform PEM -in private.pem -out private_key.pem -nocrypt
        // Read PEM file content as a string (replace with your actual reading method)
        //String pemString = readFile("/Users/apple/huflit/network programing/code/advanced-network-programming/src/main/java/Lab/Buoi_9/Bt2/private_key_pkcs8.pem");

        String pemString = readFile(privateKeyFile);

        // Remove header, footer, and newline characters

        String privateKeyPEM = pemString.replace(
                        "-----BEGIN PRIVATE KEY-----", ""
                )
                .replaceAll("\\n", "")
                .replace("-----END PRIVATE KEY-----", "");

        // Base64 decode the key data
        byte[] decodedPrivateKey = Base64.getDecoder().decode(privateKeyPEM);
        PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(decodedPrivateKey);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
        System.out.println(">>>>>>>>>" + privateKey);
        // Tạo đối tượng chữ ký
        Signature signature = Signature.getInstance("SHA256withRSA");

        // Khởi tạo chữ ký với private key
        signature.initSign(privateKey);

        // Cập nhật dữ liệu cần ký
        signature.update(message.getBytes());

        // Tạo chữ ký
        return signature.sign();
    }


    public static void BT2() throws Exception {
        String message = "acd";
        String privateKeyFile = "/Users/apple/huflit/network programing/code/advanced-network-programming/src/main/java/Lab/Buoi_9/Bt2/private_key_pkcs8.pem";
        String publicKeyFile = "/Volumes/SSD/huflit/network programing/code/advanced-network-programming/src/main/java/Lab/Buoi_9/Bt2/public_key.pem";

        try {
            byte[] signatureBytes = sign(privateKeyFile, message);

            // Kết nối đến server và gửi chuỗi BASE64
            Socket socket = new Socket("localhost", 5001);
            PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
            //System.out.println(Arrays.toString(signatureBytes));
            printWriter.println(Base64.getEncoder().encodeToString(signatureBytes));
            //byte[] decoded = Base64.getDecoder().decode(Base64.getEncoder().encodeToString(signatureBytes));
            //System.out.println(Arrays.toString(decoded));
            printWriter.println(message);
            printWriter.flush();

            // Đóng kết nối
            socket.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        BT2();
    }


}
