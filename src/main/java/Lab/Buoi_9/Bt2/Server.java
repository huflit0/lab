package Lab.Buoi_9.Bt2;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.*;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;

public class Server {
    private static InputStream inputStream;

    public static void main(String[] args) throws Exception {
        // Tạo ServerSocket để lắng nghe kết nối
        ServerSocket serverSocket = new ServerSocket(5001);

        while (true) {
            // Chấp nhận kết nối từ client
            Socket socket = serverSocket.accept();

            BT2(socket);

            // Đóng kết nối
            socket.close();
        }
    }

    public static void BT1(Socket socket) throws IOException {
        // Nhận chuỗi BASE64 từ client
        inputStream = socket.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String messageFromClient = reader.readLine();

        // Giải mã chuỗi BASE64 sang SHA-256
        byte[] decodedBytes = Base64.getDecoder().decode(messageFromClient);

        // In ra SHA256
        StringBuilder sb = new StringBuilder();
        for (byte b : decodedBytes) {
            sb.append(String.format("%02x", b));
        }
        System.out.println("Chuỗi SHA-256: " + sb.toString());

        // In ra base64 từ client gửi lên
        System.out.println("Chuỗi BASE64: " + messageFromClient);
    }

    public static void BT2(Socket socket) throws Exception {
        inputStream = socket.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String signatureFromClient = reader.readLine();
        byte[] signatureFromClientDecoded = Base64.getDecoder().decode(signatureFromClient);
        //String messageFromClient = reader.readLine();
        String messageFromClient = "abcd";
        System.out.println(Arrays.toString(signatureFromClient.getBytes()));
        String publicKeyFile = "/Users/apple/huflit/network programing/code/advanced-network-programming/src/main/java/Lab/Buoi_9/Bt2/public_key.pem";
        boolean isValid = verify(publicKeyFile, messageFromClient, signatureFromClientDecoded);

        if (isValid) {
            System.out.println("Chữ ký hợp lệ");

            //
        } else {
            System.out.println("Chữ ký không hợp lệ");
        }
    }

    public static boolean verify(String publicKeyFile,
                                 String message,
                                 byte[] signatureBytes) throws Exception {

        String pemString = readFile(publicKeyFile);

        // Remove header, footer, and newline characters
        String publicKeyPEM = pemString.replace("-----BEGIN PUBLIC KEY-----", "")
                .replaceAll("\\n", "")
                .replace("-----END PUBLIC KEY-----", "");

        byte[] publicKeyPEMBytes = Base64.getDecoder().decode(publicKeyPEM);

        X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyPEMBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

        // Tạo đối tượng chữ ký
        Signature signature = Signature.getInstance("SHA256withRSA");

        // Khởi tạo xác minh chữ ký với public key
        signature.initVerify(publicKey);

        // Cập nhật dữ liệu cần xác minh
        signature.update(message.getBytes());

        // Xác minh chữ ký
        return signature.verify(signatureBytes);
    }

    public static String readFile(String filePath) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (FileReader fr = new FileReader(filePath)) {
            int c;
            while ((c = fr.read()) != -1) {
                sb.append((char) c);
            }
        }
        return sb.toString();
    }
}