package Lab.Buoi_9.Bt1;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.security.*;
import java.util.Base64;
import java.util.Scanner;
public class Client {

	public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
		// Nhập thông điệp từ người dùng
        System.out.print("Nhập thông điệp: ");
        Scanner scan = new Scanner(System.in);
        String message = scan.nextLine();
     // Mã hóa thông điệp bằng SHA-256
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        byte[] digest = messageDigest.digest(message.getBytes());

        // In ra chuỗi sau khi SHA-256
        System.out.print("Chuỗi sau khi SHA-256: ");
        StringBuilder sb = new StringBuilder();
        for (byte b : digest) {
            sb.append(String.format("%02x", b));
        }
        System.out.println(sb.toString());

        // Chuyển đổi sang Base64
        String base64Encoded = Base64.getEncoder().encodeToString(digest);

        // In ra chuỗi sau khi Base64
        System.out.println("Chuỗi sau khi Base64: " + base64Encoded);

        // Kết nối đến server và gửi chuỗi BASE64
        Socket socket = new Socket("localhost", 5000);
        PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
        printWriter.println(base64Encoded);
        System.out.print(digest);
        printWriter.flush();

        // Đóng kết nối
        socket.close();

	}

}
