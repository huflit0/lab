package Lab.Buoi_9.Bt1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.*;
import java.util.Base64;

public class Server {

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        // Tạo ServerSocket để lắng nghe kết nối
        ServerSocket serverSocket = new ServerSocket(5000);

        while (true) {
            // Chấp nhận kết nối từ client
            Socket socket = serverSocket.accept();

            // Nhận chuỗi BASE64 từ client
            InputStream inputStream = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String messageFromClient = reader.readLine();
			
			// Giải mã chuỗi BASE64 sang SHA-256
            byte[] decodedBytes = Base64.getDecoder().decode(messageFromClient);
            
            // In ra SHA256
            StringBuilder sb = new StringBuilder();
            for (byte b : decodedBytes) {
                sb.append(String.format("%02x", b));
            }
            System.out.println("Chuỗi SHA-256: " + sb.toString());
            
            // In ra base64 từ client gửi lên
            System.out.println("Chuỗi BASE64: " + messageFromClient);

            // Đóng kết nối
            socket.close();
        }
    }
}

//note https://cryptotools.net/rsagen