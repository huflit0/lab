package Lab.Buoi_8.Bt2.Rabbit.Client;

import com.rabbitmq.client.*;

public class Client {
    private final static String QUEUE_NAME = "hello";
    private static final String EXCHANGE_NAME = "test-fanout";
    private static String message = "";

    public static int AddCal(int a, int b) {
        return a + b;
    }

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("54.169.244.117");
        factory.setUsername("admin");
        factory.setPassword("admin");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        //test fanout
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        //test fanout
        //channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "");
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message + "'");

            if (message.equals("ADD_CAL")) {
                System.out.println("Add Calculator: " + AddCal(1, 2));
            }
        };


        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> {
        });
    }
}
