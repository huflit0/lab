package Lab.Buoi_8.Bt1.RMI.Client;

import Lab.Buoi_8.Bt1.RMI.Util.IBook;
import Lab.Buoi_8.Bt1.RMI.Util.ICalculator;
import Lab.Buoi_8.Bt1.RMI.Util.IFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Arrays;

public class RMIClient {

    public static void main(String[] args) {
        try {
            ICalculator iCalculator = (ICalculator) Naming.lookup("rmi://localhost:9090/AddRMI");
            System.out.println("result: " + iCalculator.AddTwoNum(2, 2));

            //IFile iFile = (IFile) Naming.lookup("rmi://127.0.0.1:10001/ListFiles");
            //System.out.println("Files: "+ Arrays.toString(iFile.listFile("/Volumes/SSD/huflit/network programing/code/advanced-network-programming")));

//            IBook iBook = (IBook) Naming.lookup("rmi://127.0.0.1:10001/CreateBook");
//            iBook.createBook("HUFLIT TEST", "HUFLIT TEST");
//
//            iBook = (IBook) Naming.lookup("rmi://127.0.0.1:10001/ListBook");
//            iBook.listBook();
//
//            iBook = (IBook) Naming.lookup("rmi://127.0.0.1:10001/UpdateBook");
//            iBook.updateBook("hahahaha", "hihihihi", 1);
//
//            iBook = (IBook) Naming.lookup("rmi://127.0.0.1:10001/DeleteBook");
//            iBook.deleteBook("hahahaha");


        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
