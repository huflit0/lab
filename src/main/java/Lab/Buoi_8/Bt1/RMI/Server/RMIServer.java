package Lab.Buoi_8.Bt1.RMI.Server;

import Lab.Buoi_8.Bt1.RMI.Util.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.sql.SQLException;

public class RMIServer {
    public static void main(String[] args) {
        try {
            LocateRegistry.createRegistry(9090);
            ICalculator iCalculator = new CalculatorService();
            Naming.bind("rmi://localhost:9090/AddRMI", iCalculator);

//            IFile iFile = new FileService();
//            Naming.bind("rmi://localhost:10002/ListFiles", iFile);

//            IBook ibook = new BookService();
//            LocateRegistry.createRegistry(10003);
//            Naming.bind("rmi://127.0.0.1:10003/CreateBook", ibook);
//            Naming.bind("rmi://127.0.0.1:10003/ListBook", ibook);
//            Naming.bind("rmi://127.0.0.1:10003/UpdateBook", ibook);
//            Naming.bind("rmi://127.0.0.1:10003/DeleteBook", ibook);
        } catch (IOException | AlreadyBoundException e) {
//            throw new RuntimeException(e);
            e.printStackTrace();
        }
    }
}