package Lab.Buoi_8.Bt1.RMI.Util;
import java.rmi.Remote;
import java.rmi.RemoteException;
public interface ICalculator extends Remote {
    public int AddTwoNum(int a, int b) throws RemoteException;
}
