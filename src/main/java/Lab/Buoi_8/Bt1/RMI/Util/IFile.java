package Lab.Buoi_8.Bt1.RMI.Util;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IFile extends Remote {
    public String[] listFile(String directory) throws RemoteException;
}
