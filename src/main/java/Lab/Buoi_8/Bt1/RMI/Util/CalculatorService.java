package Lab.Buoi_8.Bt1.RMI.Util;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class CalculatorService extends UnicastRemoteObject implements ICalculator {

    public CalculatorService() throws RemoteException {
    }

    @Override
    public int AddTwoNum(int a, int b) throws RemoteException {
        return a * b;
    }
}
