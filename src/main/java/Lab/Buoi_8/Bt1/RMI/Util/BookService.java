package Lab.Buoi_8.Bt1.RMI.Util;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.*;

//CREATE DATABASE EBook;
//CREATE TABLE Book (
//id INT PRIMARY KEY AUTO_INCREMENT,  -- Add an auto-incrementing ID as the primary key (optional)
//name VARCHAR(255) NOT NULL,
//author VARCHAR(255) NOT NULL
//);

//CREATE USER 'user' IDENTIFIED BY '123456';
//GRANT INSERT, SELECT, DELETE, UPDATE ON EBook.* TO 'user'@'%';
//FLUSH PRIVILEGES;

public class BookService extends UnicastRemoteObject implements IBook {
    private static Connection connection;

    // Kết nối đến server và gửi chuỗi BASE64

    public BookService() throws IOException, SQLException {
//        try {
//            initDatabaseConnection();
//        } finally {
//            if (connection != null) {
//                closeDatabaseConnection();
//            }
//        }


        initDatabaseConnection();

    }

    private static void initDatabaseConnection() throws SQLException, IOException {
        System.out.println("Connecting to the database...");
        connection = DriverManager.getConnection(
                "jdbc:mariadb://127.0.0.1:3306/EBook",
                "user", "123456");

        System.out.println("Connection valid: " + connection.isValid(5));
    }

    private static void closeDatabaseConnection() throws SQLException {
        System.out.println("Closing database connection...");
        connection.close();
        System.out.println("Connection valid: " + connection.isValid(5));
    }

    @Override
    public void listBook() throws IOException, SQLException {

        try (PreparedStatement statement = connection.prepareStatement("""
				    SELECT name, author
				    FROM Book
				    ORDER BY name DESC
				""")) {
            try (ResultSet resultSet = statement.executeQuery()) {
                boolean empty = true;
                while (resultSet.next()) {
                    empty = false;
                    String name = resultSet.getString("name");
                    String author = resultSet.getString("author");
                    System.out.println("\t> " + name + ": " + author);

                }
                if (empty) {
                    System.out.println("\t (no data)");
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void createBook(String bookName, String author) throws RemoteException {
        try (PreparedStatement statement = connection.prepareStatement("""
				    INSERT INTO Book(name, author)
				    VALUES (?, ?)
				""")) {
            statement.setString(1, bookName);
            statement.setString(2, author);
            int rowsInserted = statement.executeUpdate();
            System.out.println("Rows inserted: " + rowsInserted);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteBook(String bookName) throws RemoteException {
        try (PreparedStatement statement = connection.prepareStatement("""
				    DELETE FROM Book
				    WHERE name LIKE ?
				""")) {
            statement.setString(1, bookName);
            int rowsDeleted = statement.executeUpdate();
            System.out.println("Rows deleted: " + rowsDeleted);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateBook(String bookName, String author, int id) throws RemoteException {
        try (PreparedStatement statement = connection.prepareStatement("""
				    UPDATE Book
				    SET name = ?, author = ?
				    WHERE id = ?
				""")) {

            statement.setString(1, bookName);
            statement.setString(2, author);
            statement.setInt(3, id);


            int rowsUpdated = statement.executeUpdate();
            System.out.println("Rows updated: " + rowsUpdated);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
