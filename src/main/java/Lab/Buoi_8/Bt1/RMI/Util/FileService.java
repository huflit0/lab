package Lab.Buoi_8.Bt1.RMI.Util;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class FileService extends UnicastRemoteObject implements IFile{
    public FileService() throws RemoteException {
    }

    @Override
    public String[] listFile(String directory) throws RemoteException {
        File dir = new File(directory);
        return dir.list();
    }
}
