package Lab.Buoi_8.Bt1.RMI.Util;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;

public interface IBook extends Remote {
    public void listBook() throws IOException, SQLException;
    public void createBook(String bookName, String author) throws RemoteException;
    public void deleteBook(String bookName) throws RemoteException;
    public void updateBook(String bookName, String author, int id) throws RemoteException;
}
