package Lab.Buoi_5;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class AsyncImageDownloader {
	private static final int NUM_IMAGES = 5;

    public static void main(String[] args) throws IOException {
    	// Nhập vị trí lưu trữ từ người dùng
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập vị trí lưu trữ hình ảnh: ");
        String savePath = scanner.nextLine();

        // Danh sách URL hình ảnh
        List<String> imageUrls = new ArrayList<>();
        imageUrls.add("https://letsenhance.io/static/8f5e523ee6b2479e26ecc91b9c25261e/1015f/MainAfter.jpg");
        imageUrls.add("https://img.freepik.com/free-photo/painting-mountain-lake-with-mountain-background_188544-9126.jpg");
        imageUrls.add("https://img.freepik.com/premium-photo/mountain-lake-with-mountain-background_901003-24960.jpg");
        imageUrls.add("https://www.simplilearn.com/ice9/free_resources_article_thumb/what_is_image_Processing.jpg");
        imageUrls.add("https://statusneo.com/wp-content/uploads/2023/02/MicrosoftTeams-image551ad57e01403f080a9df51975ac40b6efba82553c323a742b42b1c71c1e45f1.jpg");

        // Tạo ExecutorService với 5 luồng
        ExecutorService executorService = Executors.newFixedThreadPool(NUM_IMAGES);

        // Tải xuống hình ảnh bất đồng bộ
        for (String imageUrl : imageUrls) {
            executorService.submit(() -> {
                try {
                    // Tải xuống hình ảnh
                    URL url = new URL(imageUrl);
                    ReadableByteChannel readableChannel = Channels.newChannel(url.openStream());

                    // Tạo tên tệp từ URL
                    String fileName = imageUrl.substring(imageUrl.lastIndexOf('/') + 1);

                    // Tạo tệp lưu trữ
                    File file = new File(savePath + File.separator  + fileName);
                    try (FileOutputStream outputStream = new FileOutputStream(file)) {
                        outputStream.getChannel().transferFrom(readableChannel, 0, Long.MAX_VALUE);
                    }

                    // Hiển thị thông báo tải xuống
                    System.out.println("Downloaded image: " + file.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }

        // ExecutorService tự động đóng khi tất cả các tác vụ hoàn thành
    }
}
