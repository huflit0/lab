package Lab.Buoi_5;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Client {
	private static final int PORT = 8080;
    private static final String HOST = "localhost";

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        // Nhập văn bản cần tìm kiếm
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập văn bản cần tìm kiếm: ");
        String searchText = scanner.nextLine();

        // Nhập danh sách tệp văn bản
        System.out.print("Nhập danh sách tệp văn bản (cách nhau bởi dấu phẩy): ");
        String fileNames = scanner.nextLine();
        List<String> fileNamesList = new ArrayList<>();
        for (String fileName : fileNames.split(",")) {
            fileNamesList.add(fileName.trim());
        }

        // Tạo socket kết nối với server
        Socket socket = new Socket(HOST, PORT);

        // Gửi thông tin tìm kiếm đến server
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
        oos.writeObject(searchText);
        oos.writeObject(fileNamesList);
        oos.flush();

        // Nhận kết quả tìm kiếm từ server
        for (String fileName : fileNamesList) {
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            int count = (int) ois.readObject();

            // Hiển thị kết quả
            System.out.println("Tệp: " + fileName);
            System.out.println("Số lần xuất hiện: " + count);
        }

        // Đóng socket
        socket.close();
    }
}
