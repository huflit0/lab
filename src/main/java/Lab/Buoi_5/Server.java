package Lab.Buoi_5;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Server {
	private static final int PORT = 8080;

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        try (// Tạo socket server
        		
		ServerSocket serverSocket = new ServerSocket(PORT)) {
        	System.out.println("server is running");
			while (true) {
			    // Chấp nhận kết nối từ client
			    Socket socket = serverSocket.accept();

			    // Nhận văn bản cần tìm kiếm và danh sách tệp văn bản từ client
			    ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			    String searchText = (String) ois.readObject();
			    List<String> files = (List<String>) ois.readObject();

			    // Tìm kiếm văn bản trong các tệp và gửi kết quả cho client
			    for (String file : files) {
			        int count = countOccurrences(file, searchText);
			        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			        oos.writeObject(count);
			        oos.flush();
			    }

			    // Đóng socket
			    socket.close();
			}
		}
    }

    private static int countOccurrences(String file, String searchText) throws IOException {
        AtomicInteger count = new AtomicInteger(0);
        Files.lines(new File(file).toPath()).forEach(line -> {
            if (line.contains(searchText)) {
                count.incrementAndGet();
            }
        });
        return count.get();
    }
}
