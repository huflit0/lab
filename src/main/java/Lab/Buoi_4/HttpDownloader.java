package Lab.Buoi_4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class HttpDownloader {
	private static final int BUFFER_SIZE = 4096;

	public static void downloadFile(String fileURL, String saveDir) throws IOException {
		URL url = new URL(fileURL);
		HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
		httpConn.setRequestMethod("GET");
		httpConn.connect();
		
		int responseCode = httpConn.getResponseCode();
		if (responseCode != HttpURLConnection.HTTP_OK) {
			throw new IOException("Error downloading file: " + responseCode);
		}

		String fileName = "";
		String disposition = httpConn.getHeaderField("Content-Disposition");
		String contentType = httpConn.getContent().toString();
		int contentLength = httpConn.getContentLength();
        long fileSize = httpConn.getContentLengthLong();

		if (disposition != null) {
			int index = disposition.indexOf("filename=");
			if (index > 0) {
				fileName = disposition.substring(index + 10, disposition.length() - 1);
			}
		} else {
			fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1, fileURL.length());
		}
		
		System.out.println("Content-Type: " + contentType);
		System.out.println("Content-Disposition: " + disposition);
		System.out.println("Content-Length: " + contentLength);

		System.out.println("fileName: " + fileName);

		
		
		// save file
		InputStream inputStream = httpConn.getInputStream();
		String saveFilePath = saveDir + File.separator + fileName;
		FileOutputStream outputStream = new FileOutputStream(saveFilePath);
		int bytesRead;
		long totalDownloaded = 0;
		
		byte[] buffer = new byte[BUFFER_SIZE];
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			outputStream.write(buffer, 0, bytesRead);
			
			// Print download progress 
			totalDownloaded += bytesRead;
            if (fileSize > 0) {
                int progress = (int) ((totalDownloaded * 100) / fileSize);
                System.out.println("Downloaded " + progress + "%");
            }
		}
		outputStream.close();
		httpConn.disconnect();
	}
	
	public static void getHtml(String urlString) throws IOException {

        URL url = new URL(urlString);
        URLConnection connection = url.openConnection();

        // Open an input stream to read the HTML content
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        List<String> htmlContent = new ArrayList<String>();
        String line;
        while ((line = reader.readLine()) != null) {
            htmlContent.add(line);
        }
        reader.close();

        System.out.println(String.join("\n", htmlContent));
	}
	
	public static void whoIs (String domain) throws IOException {
		

        // Default WHOIS server port
        int port = 43;

        // Connect to the WHOIS server
        try (Socket socket = new Socket("whois.internic.com", port)) {
            // Send the WHOIS query
            OutputStreamWriter writer = new OutputStreamWriter(socket.getOutputStream());
            writer.write("WHOIS " + domain + "\r\n");
            writer.flush();

            // Read the response from the WHOIS server
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        }
	}
}
