package Lab.Buoi_4;

import java.io.IOException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws IOException {
		
		Scanner sc = new Scanner(System.in);
		int choice;
		do {
			System.out.println("1. Download file");
			System.out.println("2. Get Html");
			System.out.println("3. Whois");
			System.out.println("4. Exit");
			System.out.print("choose: ");
			choice = sc.nextInt();
			
			switch (choice) {
			case 1:
				String fileURL = "https://www.simplilearn.com/ice9/free_resources_article_thumb/what_is_image_Processing.jpg";
				String saveDir = "C:\\Users\\phang\\Desktop\\huflit\\Huflit\\src\\main\\java\\Lab\\Buoi_4";
				
				HttpDownloader.downloadFile(fileURL, saveDir);
				System.out.println("Download completed");
				break;
			case 2:
				System.out.print("Enter Url");
				String urlString = sc.next();
				HttpDownloader.getHtml(urlString);
				break;
			case 3:
				System.out.print("Enter domain: ");
		        String domain = sc.next();
		        HttpDownloader.whoIs(domain);
				break;
			default:
				break;
			}
		} while (choice < 4);
		
		System.out.print("Done!");
	}

}
